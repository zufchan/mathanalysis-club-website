<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">

    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

  </head>
  <body>
    <nav class="navbar navbar-expand-lg bg-dark navbar-dark fixed-top">
      <!-- Brand -->
      <a class="navbar-brand" href="mainpage.php"><img src="Logo..png" alt="" height="50px" ></a>

      <!-- Toggler/collapsibe Button -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Navbar links -->
      <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="geometry.php">Geometry</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="modar.php">Modular Arithmetic</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="funcs.php">Functions</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Calculus I
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="lim1.php">Limits</a>
              <a class="dropdown-item" href="diff1.php">Differentiation</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href ="int1-1.php">Simple Integration</a>
              <a class="dropdown-item" href="int1-2.php">Integration by substitution</a>
              <a class="dropdown-item" href="int1-3.php">Integration by parts</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Calculus II
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="lim2.php">Limits</a>
              <a class="dropdown-item" href="diff2.php">Differentiation</a>
              <a class="dropdown-item" href="int2.php">Double Integrals</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="forum.php">Forum</a>
          </li>
          <li class="nav-item dropdown" style="position: absolute; right: 15px;">

            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php $time = explode(" ", $_COOKIE["date"]);
                    $time2 = explode(":" , $time[1]);
                    $time2[0] = (intval($time2[0])+4)%24;
               ?>
               <span class="badge badge-info"><?php echo $time2[0].":".$time2[1]; ?></span>
              <?php echo "  ".$login_session." "; ?>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="profile.php">Profile</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="logout.php">Log out</a>
            </div>


          </li>
        </ul>
      </div>
    </nav>
  </body>
</html>
