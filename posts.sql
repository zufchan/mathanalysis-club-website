-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июн 20 2020 г., 17:25
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `final`
--

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `post_id` int(100) NOT NULL,
  `post_name` varchar(200) NOT NULL,
  `post_content` text NOT NULL,
  `date` varchar(20) NOT NULL,
  `user_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`post_id`, `post_name`, `post_content`, `date`, `user_id`) VALUES
(1, 'First post', 'Congratulations dear students! The forum is now open. Please, do not hesitate to post on this webpage. Posts that do not obey the rules, however, will be deleted. <br> <br>\r\nPlease, stay home during COVID-19.', '19.06.2020, 20:39', 1),
(2, 'Functional equation problem', '$$\\textbf{For some continious function } f:R->R \\textbf{ it is given that } 9f(x+y)=f(x)(y),$$\r\n$$\\textbf{for all real numbers } x \\textbf{ and } y. \\textbf{ what is the value of } f(-f(1)) \\textbf{, if } f(1)=3?$$<br>\r\nProblem was taken from http://mathforces.com/problems/82/', '20.06.2020, 18:08', 1),
(3, 'First post of Kassym', 'The virus problem interferes with learning processes. A problem is that it spreads very fast. Due to that many countries decided to prevent all of the events that may contain a big amount of people in one place including education processes. In the case of Kazakhstan, from 16th march school activity was stopped completely and students from universities were introduced to distant learning. For some students, it will be harder to force themselves to study at home. Since Kazakhstanish institutions weren’t ready for that massive changes, students now face a problem with a lack of education.\r\n', '20.06.2020, 20:15', 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
