<?php include('session.php') ?>
<?php include('navbar.php') ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Calculus II - Differentiation - AITU MathAnalysis</title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <style media="screen">

          body {
          background-color: black;
          font-family: Arial;
          display: block;
          background-image: url(repeated-square-dark.png);
          background-position: center;
          padding: 0;
          margin: 0;
          }
          .sk-cube-grid {
            width: 100px;
            height: 100px;
            position:fixed;
            margin-left: 47%;
            margin-top: 30vh;
            animation: moving1 linear;
            animation-duration: 0.5s;
            animation-delay: 1.5s;
            animation-fill-mode: both;
            opacity: 0.8;
          }
          @keyframes moving1 {
              0% {
              opacity: 0.8; z-index: 1;
              }
              100% {
              opacity: 0; z-index: -1;
            }
          }
          @keyframes moving {
              0% {
              opacity: 0; z-index: -1;
              }
              100% {
              opacity: 1; z-index: 1;
            }
          }
          .contentdiv{
            animation: moving linear;
            animation-duration: 1s;
            animation-delay: 2s;
            animation-fill-mode: both;
          }
          .sk-cube-grid .sk-cube {
            border-radius: 5px;
            width: 33%;
            height: 33%;
            float: left;
            -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation-iteration-count: 2;
          }
          .sk-cube-grid .sk-cube1 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                    background-color: blue; }
          .sk-cube-grid .sk-cube2 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                    background-color: lime;}
          .sk-cube-grid .sk-cube3 {
            -webkit-animation-delay: 0.4s;
                    animation-delay: 0.4s;
                  background-color: red;}
          .sk-cube-grid .sk-cube4 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: lightblue; }
          .sk-cube-grid .sk-cube5 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: purple; }
          .sk-cube-grid .sk-cube6 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                  background-color: pink; }
          .sk-cube-grid .sk-cube7 {
            -webkit-animation-delay: 0s;
                    animation-delay: 0s;
                  background-color: yellow;}
          .sk-cube-grid .sk-cube8 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: orange; }
          .sk-cube-grid .sk-cube9 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: rgb(100,200,300);}

          @-webkit-keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }
          }

          @keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }

          }
          .row{
            width: 100%;
            padding: 1%;
            display: flex;
            flex-direction: row;
            justify-content: space-around;
          }
          @keyframes fadeapp {
            from {opacity: 0;}
            to {opacity: 1;}
          }
          .imga{
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 3s;
            animation-delay: 2.5s;
            animation-fill-mode: forwards;
          }
          .aligncenter {
            text-align: center;
          }
          .textdiv {
            margin: 20px;
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 1s;
            animation-fill-mode: forwards;
            background-color: rgba(8, 43, 112, 0.5);
            color: white;
            border: white solid 3px;
            border-radius: 5px;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            padding: 10px;
            height: 70%;
          }
          .footer11{
            padding: 10px;
            background-color: rgb(38, 45, 66);
            width: 100%;
            color: rgb(88, 89, 92);
          }
          .logos{
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
          }
          .logo{
            margin-right: 20px;
          }
    </style>

  </head>
  <body>

    <div class="textdiv" style="margin-top: 100px;">
      <h1 class="aligncenter">Partial Derivatives</h1>
      <span class="example-title">Examples</span> Find all of the first order partial derivatives for the following functions.
<ol class="example_parts_list">
  <li>\(f\left( {x,y} \right) = {x^4} + 6\sqrt y  - 10\)</li>
  <li>\(w = {x^2}y - 10{y^2}{z^3} + 43x - 7\tan \left( {4y} \right)\)</li>
  <li>\(\displaystyle h\left( {s,t} \right) = {t^7}\ln \left( {{s^2}} \right) + \frac{9}{{{t^3}}} - \sqrt[7]{{{s^4}}}\)</li>
  <li>\(\displaystyle f\left( {x,y} \right) = \cos \left( {\frac{4}{x}} \right){{\bf{e}}^{{x^2}y - 5{y^3}}}\)</li>
</ol>
<button style="width:200px;" class="btn btn-primary" data-toggle="collapse" data-target="#demo1">Show Solution</button>
<br>
<div id="demo1" class="collapse">
  <div class="textdiv col-lg-12 col-md-12" style="margin: auto; background-color: rgba(170,120,140,0.3);">
    <div class="textdiv col-lg-8 col-md-12" style="margin: auto; background-color: rgba(170,190,140,0.3);">
<h5>Example 1</h5>
      <p>Let’s first take the derivative with respect to \(x\) and remember that as we do so all the \(y\)’s will be treated as constants.  The partial derivative with respect to \(x\) is,</p>

           \[{f_x}\left( {x,y} \right) = 4{x^3}\]

         <p>Notice that the second and the third term differentiate to zero in this case.  It should be clear why the third term differentiated to zero.  It’s a constant and we know that constants always differentiate to zero.  This is also the reason that the second term differentiated to zero.  Remember that since we are differentiating with respect to \(x\) here we are going to treat all \(y\)’s as constants.  That means that terms that only involve \(y\)’s will be treated as constants and hence will differentiate to zero.</p>

         <p>Now, let’s take the derivative with respect to \(y\).  In this case we treat all \(x\)’s as constants and so the first term involves only \(x\)’s and so will differentiate to zero, just as the third term will.  Here is the partial derivative with respect to \(y\).</p>

             \[{f_y}\left( {x,y} \right) = \frac{3}{{\sqrt y }}\]
    </div>
    <div class="textdiv col-lg-8 col-md-12" style="margin: auto; background-color: rgba(170,190,140,0.3);">
<h5>Example 2</h5>
<p>With this function we’ve got three first order derivatives to compute.  Let’s do the partial derivative with respect to \(x\) first.  Since we are differentiating with respect to \(x\) we will treat all \(y\)’s and all \(z\)’s as constants.  This means that the second and fourth terms will differentiate to zero since they only involve \(y\)’s and \(z\)’s.</p>

  <p>This first term contains both \(x\)’s and \(y\)’s and so when we differentiate with respect to \(x\) the \(y\) will be thought of as a multiplicative constant and so the first term will be differentiated just as the third term will be differentiated.</p>

  <p>Here is the partial derivative with respect to \(x\).</p>

      \[\frac{{\partial w}}{{\partial x}} = 2xy + 43\]

  <p>Let’s now differentiate with respect to \(y\).  In this case all \(x\)’s and \(z\)’s will be treated as constants.  This means the third term will differentiate to zero since it contains only \(x\)’s while the \(x\)’s in the first term and the \(z\)’s in the second term will be treated as multiplicative constants.  Here is the derivative with respect to \(y\).</p>

      \[\frac{{\partial w}}{{\partial y}} = {x^2} - 20y{z^3} - 28{\sec ^2}\left( {4y} \right)\]

  <p>Finally, let’s get the derivative with respect to \(z\).  Since only one of the terms involve \(z\)’s this will be the only non-zero term in the derivative.  Also, the \(y\)’s in that term will be treated as multiplicative constants.  Here is the derivative with respect to \(z\).</p>

      \[\frac{{\partial w}}{{\partial z}} =  - 30{y^2}{z^2}\]
    </div>
    <div class="textdiv col-lg-8 col-md-12" style="margin: auto; background-color: rgba(170,190,140,0.3);">
<h5>Example 3</h5>
<p>With this one we’ll not put in the detail of the first two.  Before taking the derivative let’s rewrite the function a little to help us with the differentiation process.</p>

    \[h\left( {s,t} \right) = {t^7}\ln \left( {{s^2}} \right) + 9{t^{ - 3}} - {s^{\frac{4}{7}}}\]

  <p>Now, the fact that we’re using \(s\) and \(t\) here instead of the “standard” \(x\) and \(y\) shouldn’t be a problem.  It will work the same way.  Here are the two derivatives for this function.</p>

      \[\begin{align*}{h_s}\left( {s,t} \right) & = \frac{{\partial h}}{{\partial s}} = {t^7}\left( {\frac{{2s}}{{{s^2}}}} \right) - \frac{4}{7}{s^{ - \frac{3}{7}}} = \frac{{2{t^7}}}{s} - \frac{4}{7}{s^{ - \frac{3}{7}}}\\ {h_t}\left( {s,t} \right) & = \frac{{\partial h}}{{\partial t}} = 7{t^6}\ln \left( {{s^2}} \right) - 27{t^{ - 4}}\end{align*}\]

  <p>Remember how to differentiate natural logarithms.</p>

    \[\frac{d}{{dx}}\left( {\ln g\left( x \right)} \right) = \frac{{g'\left( x \right)}}{{g\left( x \right)}}\]
    </div>
    <div class="textdiv col-lg-8 col-md-12" style="margin: auto; background-color: rgba(170,190,140,0.3);">
<h5>Example 4</h5>
<p>Now, we can’t forget the product rule with derivatives.  The product rule will work the same way here as it does with functions of one variable.  We will just need to be careful to remember which variable we are differentiating with respect to.</p>

   <p>Let’s start out by differentiating with respect to \(x\).  In this case both the cosine and the exponential contain \(x\)’s and so we’ve really got a product of two functions involving \(x\)’s and so we’ll need to product rule this up.  Here is the derivative with respect to \(x\).</p>

     \[\begin{align*}{f_x}\left( {x,y} \right) & =  - \sin \left( {\frac{4}{x}} \right)\left( { - \frac{4}{{{x^2}}}} \right){{\bf{e}}^{{x^2}y - 5{y^3}}} + \cos \left( {\frac{4}{x}} \right){{\bf{e}}^{{x^2}y - 5{y^3}}}\left( {2xy} \right)\\ &amp;  = \frac{4}{{{x^2}}}\sin \left( {\frac{4}{x}} \right){{\bf{e}}^{{x^2}y - 5{y^3}}} + 2xy\cos \left( {\frac{4}{x}} \right){{\bf{e}}^{{x^2}y - 5{y^3}}}\end{align*}\]

   <p>Do not forget the chain rule for functions of one variable.  We will be looking at the chain rule for some more complicated expressions for multivariable functions in a later section.  However, at this point we’re treating all the \(y\)’s as constants and so the chain rule will continue to work as it did back in Calculus I.</p>

   <p>Also, don’t forget how to differentiate exponential functions,</p>

     \[\frac{d}{{dx}}\left( {{{\bf{e}}^{f\left( x \right)}}} \right) = f'\left( x \right){{\bf{e}}^{f\left( x \right)}}\]

   <p>Now, let’s differentiate with respect to \(y\).  In this case we don’t have a product rule to worry about since the only place that the \(y\) shows up is in the exponential.  Therefore, since \(x\)’s are considered to be constants for this derivative, the cosine in the front will also be thought of as a multiplicative constant.  Here is the derivative with respect to \(y\).</p>

       \[{f_y}\left( {x,y} \right) = \left( {{x^2} - 15{y^2}} \right)\cos \left( {\frac{4}{x}} \right){{\bf{e}}^{{x^2}y - 5{y^3}}}\]
    </div>
  </div></div>
    </div>
    <div class="textdiv">
      <div class="row">
        <div class="col-lg-12 col-md-12 aligncenter">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/AXqhWeUEtQU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-lg-12 col-md-12 aligncenter">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/XipB_uEexF0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-lg-12 col-md-12 aligncenter">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/AXH9Xm6Rbfc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
    <?php include('footer.php') ?>

  </body>
</html>
