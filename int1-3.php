<?php include('session.php') ?>
<?php include('navbar.php') ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Calculus I - Integration by parts - AITU MathAnalysis</title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <style media="screen">

          body {
          background-color: black;
          font-family: Arial;
          display: block;
          background-image: url(repeated-square-dark.png);
          background-position: center;
          padding: 0;
          margin: 0;
          }
          .sk-cube-grid {
            width: 100px;
            height: 100px;
            position:fixed;
            margin-left: 47%;
            margin-top: 30vh;
            animation: moving1 linear;
            animation-duration: 0.5s;
            animation-delay: 1.5s;
            animation-fill-mode: both;
            opacity: 0.8;
          }
          @keyframes moving1 {
              0% {
              opacity: 0.8; z-index: 1;
              }
              100% {
              opacity: 0; z-index: -1;
            }
          }
          @keyframes moving {
              0% {
              opacity: 0; z-index: -1;
              }
              100% {
              opacity: 1; z-index: 1;
            }
          }
          .contentdiv{
            animation: moving linear;
            animation-duration: 1s;
            animation-delay: 2s;
            animation-fill-mode: both;
          }
          .sk-cube-grid .sk-cube {
            border-radius: 5px;
            width: 33%;
            height: 33%;
            float: left;
            -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation-iteration-count: 2;
          }
          .sk-cube-grid .sk-cube1 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                    background-color: blue; }
          .sk-cube-grid .sk-cube2 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                    background-color: lime;}
          .sk-cube-grid .sk-cube3 {
            -webkit-animation-delay: 0.4s;
                    animation-delay: 0.4s;
                  background-color: red;}
          .sk-cube-grid .sk-cube4 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: lightblue; }
          .sk-cube-grid .sk-cube5 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: purple; }
          .sk-cube-grid .sk-cube6 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                  background-color: pink; }
          .sk-cube-grid .sk-cube7 {
            -webkit-animation-delay: 0s;
                    animation-delay: 0s;
                  background-color: yellow;}
          .sk-cube-grid .sk-cube8 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: orange; }
          .sk-cube-grid .sk-cube9 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: rgb(100,200,300);}

          @-webkit-keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }
          }

          @keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }

          }
          .row{
            width: 100%;
            padding: 1%;
            display: flex;
            flex-direction: row;
            justify-content: space-around;
          }
          @keyframes fadeapp {
            from {opacity: 0;}
            to {opacity: 1;}
          }
          .imga{
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 3s;
            animation-delay: 2.5s;
            animation-fill-mode: forwards;
          }
          .aligncenter {
            text-align: center;
          }
          .textdiv {
            margin: 20px;
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 1s;
            animation-fill-mode: forwards;
            background-color: rgba(8, 43, 112, 0.5);
            color: white;
            border: white solid 3px;
            border-radius: 5px;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            padding: 10px;
            height: 70%;
          }
          .footer11{
            padding: 10px;
            background-color: rgb(38, 45, 66);
            width: 100%;
            color: rgb(88, 89, 92);
          }
          .logos{
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
          }
          .logo{
            margin-right: 20px;
          }
    </style>

  </head>
  <body>

    <div class="textdiv" style="margin-top: 100px;">
      <p>Integration by parts is another technique for simplifying integrands. As we saw in previous posts, each differentiation rule has a corresponding integration rule. In the case of integration by parts, the corresponding differentiation rule is the Product Rule. The technique of integration by parts allows us to simplify integrands of the form:</p>
      $$\int f(x) g(x) dx$$
      <p>Examples of this form include:</p>
      $$\int x \cos{x} \space dx, \qquad \int e^x \cos{x} \space dx, \qquad \int x^2 e^x \space dx$$
      <div class="textdiv col-lg-8 col-md-12" style="margin: auto; background-color: rgba(120,170,140,0.3);">
        <h4>Integration By Parts</h4>
        <div class="fact">
          $$u=f(x), v=g(x), du=f'(x)dx, dv=g'(x)dx$$
          \[\int{{u\,dv}} = uv - \int{{v\,du}}\]
        </div>
      </div>
      <p>Let's take a look at the example of integral that should be evaluated using integration bt parts</p>
      <span class="example-title">Example 1</span> Evaluate the following integral.

          \[\int{{x{{\bf{e}}^{6x}}\,dx}}\]
      <button style="width:200px;" class="btn btn-primary" data-toggle="collapse" data-target="#demo1">Show Solution</button>

      <div id="demo1" class="collapse">
        <div class="textdiv col-lg-8 col-md-12" style="margin: auto; background-color: rgba(170,120,140,0.3);">
          <p>So, on some level, the problem here is the \(x\) that is in front of the exponential.  If that wasn’t there we could do the integral.  Notice as well that in doing integration by parts anything that we choose for \(u\) will be differentiated.  So, it seems that choosing \(u = x\) will be a good choice since upon differentiating the \(x\) will drop out.</p>

          <p>Now that we’ve chosen \(u\) we know that \(dv\) will be everything else that remains.  So, here are the choices for \(u\) and \(dv\) as well as \(du\) and \(v\).</p>

            \[\begin{align*}u & = x & \hspace{0.5in} dv & = {{\bf{e}}^{6x}}\,dx\\ du & = dx & \hspace{0.5in} v & = \int{{{{\bf{e}}^{6x}}\,dx}} = \frac{1}{6}{{\bf{e}}^{6x}}\end{align*}\]

          <p>The integral is then,</p>

              \[\begin{align*}\int{{x{{\bf{e}}^{6x}}\,dx}} &= \frac{x}{6}{{\bf{e}}^{6x}} - \int{{\frac{1}{6}{{\bf{e}}^{6x}}\,dx}}\\ &amp;  = \frac{x}{6}{{\bf{e}}^{6x}} - \frac{1}{{36}}{{\bf{e}}^{6x}} + c\end{align*}\]

          <p>Once we have done the last integral in the problem we will add in the constant of integration to get our final answer.</p>

          <p>Note as well that, as noted above, we know we hade made a correct choice for \(u\) and \(dv\) when we got a new integral that we actually evaluate after applying the integration by parts formula.</p>
        </div>
      </div>
         <span class="example-title">Example 2</span> Evaluate the following integral.

              \[\int{{\left( {3t + 5} \right)\cos \left( {\frac{t}{4}} \right)\,dt}}\]
         <button style="width:200px;" class="btn btn-primary" data-toggle="collapse" data-target="#demo2">Show Solution</button>

         <div id="demo2" class="collapse">
            <div class="textdiv col-lg-8 col-md-12" style="margin: auto; background-color: rgba(170,120,140,0.3);">
              <p>There are two ways to proceed with this example.  For many, the first thing that they try is multiplying the cosine through the parenthesis, splitting up the integral and then doing integration by parts on the first integral.</p>

          <p>While that is a perfectly acceptable way of doing the problem it’s more work than we really need to do.  Instead of splitting the integral up let’s instead use the following choices for \(u\) and \(dv\).</p>

            \[\begin{align*}u & = 3t + 5& \hspace{0.5in}dv & = \cos \left( {\frac{t}{4}} \right)\,dt\\ du & = 3\,dt & \hspace{0.5in}v & = 4\sin \left( {\frac{t}{4}} \right)\end{align*}\]

          <p>The integral is then,</p>

            \[\begin{align*}\int{{\left( {3t + 5} \right)\cos \left( {\frac{t}{4}} \right)\,dt}} & = 4\left( {3t + 5} \right)\sin \left( {\frac{t}{4}} \right) - 12\int{{\sin \left( {\frac{t}{4}} \right)\,dt}}\\ &amp;  = 4\left( {3t + 5} \right)\sin \left( {\frac{t}{4}} \right) + 48\cos \left( {\frac{t}{4}} \right) + c\end{align*}\]

          <p>Notice that we pulled any constants out of the integral when we used the integration by parts formula.  We will usually do this in order to simplify the integral a little.</p>
            </div>
         </div>
    </div>
    <div class="textdiv">
      <div class="row">
        <div class="col-lg-5 col-md-12 aligncenter">
          <iframe width="90%" height="315" src="https://www.youtube.com/embed/8tBykI6Ndj0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-lg-5 col-md-12 aligncenter">
          <iframe width="90%" height="315" src="https://www.youtube.com/embed/matDV3XL2J8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
    <?php include('footer.php') ?>

  </body>
</html>
