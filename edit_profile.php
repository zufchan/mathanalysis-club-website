<?php include('session.php') ?>

<?php
if($_SERVER["REQUEST_METHOD"] == "POST") {
  if(isset($_POST['Description'])) $upd_desc_qry = mysqli_query($db, "Update users set description = '".$_POST['Description']."' where username = '".$login_session."'");
  if($upd_desc_qry){
    echo "<script> location.href='profile.php'; </script>";
  }else{
    echo mysqli_error($db);
  }
}
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
        <link rel="shortcut icon" href="stuff.ico">
    <title>Editing your profile</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
  </head>
  <body>
    <?php include('navbar.php') ?>

    <div class="row" style="margin-top: 100px;">
      <div class="col-lg-8 col-md-12">
        <div class="card">
          <div class="card-header">
            <h1 class="card-title">Edit your profile description: </h1>
          </div>
          <div class="card-body">
            <form  method="post">
              <div class="form-group">
                <label for="comment">Description:</label>
                <textarea class="form-control" rows="5" name="Description" value = "<?php echo $user_desc; ?>"></textarea>
              </div>
              <button type="submit" class="btn btn-success" name="button">Apply changes</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <?php include('footer.php') ?>
  </body>
</html>
