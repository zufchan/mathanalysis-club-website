<?php include('session.php') ?>
<?php include('navbar.php') ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Calculus I - Differentiation - AITU MathAnalysis</title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style media="screen">

          body {
          background-color: black;
          font-family: Arial;
          display: block;
          background-image: url(repeated-square-dark.png);
          background-position: center;
          padding: 0;
          margin: 0;
          }
          .sk-cube-grid {
            width: 100px;
            height: 100px;
            position:fixed;
            margin-left: 47%;
            margin-top: 30vh;
            animation: moving1 linear;
            animation-duration: 0.5s;
            animation-delay: 1.5s;
            animation-fill-mode: both;
            opacity: 0.8;
          }
          @keyframes moving1 {
              0% {
              opacity: 0.8; z-index: 1;
              }
              100% {
              opacity: 0; z-index: -1;
            }
          }
          @keyframes moving {
              0% {
              opacity: 0; z-index: -1;
              }
              100% {
              opacity: 1; z-index: 1;
            }
          }
          .contentdiv{
            animation: moving linear;
            animation-duration: 1s;
            animation-delay: 2s;
            animation-fill-mode: both;
          }
          .sk-cube-grid .sk-cube {
            border-radius: 5px;
            width: 33%;
            height: 33%;
            float: left;
            -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation-iteration-count: 2;
          }
          .sk-cube-grid .sk-cube1 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                    background-color: blue; }
          .sk-cube-grid .sk-cube2 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                    background-color: lime;}
          .sk-cube-grid .sk-cube3 {
            -webkit-animation-delay: 0.4s;
                    animation-delay: 0.4s;
                  background-color: red;}
          .sk-cube-grid .sk-cube4 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: lightblue; }
          .sk-cube-grid .sk-cube5 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: purple; }
          .sk-cube-grid .sk-cube6 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                  background-color: pink; }
          .sk-cube-grid .sk-cube7 {
            -webkit-animation-delay: 0s;
                    animation-delay: 0s;
                  background-color: yellow;}
          .sk-cube-grid .sk-cube8 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: orange; }
          .sk-cube-grid .sk-cube9 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: rgb(100,200,300);}

          @-webkit-keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }
          }

          @keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }

          }
          .row{
            width: 100%;
            display: flex;
            flex-direction: row;
            justify-content: space-around;
          }
          @keyframes fadeapp {
            from {opacity: 0;}
            to {opacity: 1;}
          }
          .imga{
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 3s;
            animation-delay: 2.5s;
            animation-fill-mode: forwards;
          }
          .aligncenter {
            text-align: center;
          }
          .textdiv {
            margin: 20px;
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 1s;
            animation-fill-mode: forwards;
            background-color: rgba(8, 43, 112, 0.5);
            color: white;
            border: white solid 3px;
            border-radius: 5px;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            padding: 10px;
            height: 70%;
          }
          .footer11{
            padding: 10px;
            background-color: rgb(38, 45, 66);
            width: 100%;
            color: rgb(88, 89, 92);
          }
          .logos{
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
          }
          .logo{
            margin-right: 20px;
          }
    </style>

  </head>
  <body>
    
    <div class="textdiv" style="margin-top: 100px; background-color: rgba(120, 200, 160, 0.3);">
      <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
      <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
      <h1 class="aligncenter">Table of Derivatives</h1>
      <table border="9px dotted white" class="aligncenter">
        <tr>
          <td><b>$$y = f(x) $$</b></td><td><b>$$y' = \frac{dx}{dy} = f'(x) $$</b></td>
        </tr>
        <tr>
          <td>$$k$$</td><td>0</td>
        </tr>
        <tr>
          <td>$$x$$</td><td>1</td>
        </tr>
        <tr>
          <td>$$x^n$$</td><td>$$nx^{n-1}$$</td>
        </tr>
        <tr>
          <td>$$e^x$$</td><td>$$e^x$$</td>
        </tr>
        <tr>
          <td>$$e^{kx}$$</td><td>$$ke^{kx}$$</td>
        </tr>
        <tr>
          <td>$$ln(x)$$</td><td>$$\frac{1}{x}$$</td>
        </tr>
        <tr>
          <td>$$sin(kx)$$</td><td>$$kcos(kx)$$</td>
        </tr>
        <tr>
          <td>$$cos(kx)$$</td><td>$$-ksin(kx)$$</td>
        </tr>
        <tr>
          <td>$$tan(kx)$$</td><td>$$ksec^2(kx)$$</td>
        </tr>
        <tr>
          <td>$$cosec(x)$$</td><td>$$-cosec(x)cot(x)$$</td>
        </tr>
        <tr>
          <td>$$sec(x)$$</td><td>$$sec(x)tan(x)$$</td>
        </tr>
        <tr>
          <td>$$cot(x)$$</td><td>$$-cosec^2(x)$$</td>
        </tr>
        <tr>
          <td>$$sin^{-1}(x)$$</td><td>$$\frac{1}{\sqrt{1-x^2}}$$</td>
        </tr>
        <tr>
          <td>$$cos^{-1}(x)$$</td><td>$$\frac{-1}{\sqrt{1-x^2}}$$</td>
        </tr>
        <tr>
          <td>$$tan^{-1}(x)$$</td><td>$$\frac{1}{1+x^2}$$</td>
        </tr>
        <tr>
          <td>$$cosh(x)$$</td><td>$$sinh(x)$$</td>
        </tr>
        <tr>
          <td>$$sinh(x)$$</td><td>$$cosh(x)$$</td>
        </tr>
        <tr>
          <td>$$tanh(x)$$</td><td>$$sech^2(x)$$</td>
        </tr>
        <tr>
          <td>$$sech(x)$$</td><td>$$-sech(x)tanh(x)$$</td>
        </tr>
        <tr>
          <td>$$cosech(x)$$</td><td>$$-cosech(x)coth(x)$$</td>
        </tr>
        <tr>
          <td>$$coth(x)$$</td><td>$$-cosech^2(x)$$</td>
        </tr>
        <tr>
          <td>$$cosh^{-1}(x)$$</td><td>$$\frac{1}{\sqrt{x^2-1}}$$</td>
        </tr>
        <tr>
          <td>$$sinh^{-1}(x)$$</td><td>$$\frac{1}{\sqrt{x^2+1}}$$</td>
        </tr>
        <tr>
          <td>$$tanh^{-1}(x)$$</td><td>$$\frac{1}{1-x^2}$$</td>
        </tr>
      </table>
    </div>
    <div class="textdiv">
      <div class="row">
        <div class="col-md-12 col-lg-5 aligncenter">
          <iframe width="90%" height="315" src="https://www.youtube.com/embed/9vKqVkMQHKk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-md-12 col-lg-5 aligncenter">
          <iframe width="90%" height="315" src="https://www.youtube.com/embed/S0_qX4VJhMQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      <div class="row">
        <div class="col-md-11 col-lg-11 aligncenter">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/YG15m2VwSjA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-lg-5 aligncenter">
          <iframe width="90%" height="315" src="https://www.youtube.com/embed/m2MIpDrF7Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-md-12 col-lg-5 aligncenter">
          <iframe width="90%" height="315" src="https://www.youtube.com/embed/qb40J4N1fa4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      <div class="row">
        <div class="col-md-11 col-lg-11 aligncenter">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/BLkz5LGWihw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>

    <?php include('footer.php') ?>
  </body>
</html>
