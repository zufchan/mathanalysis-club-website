<?php
   include('session.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>AITU MathAnalysis</title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style media="screen">

      body {
      background-color: black;
      font-family: Arial;
      display: block;
      background-image: url(repeated-square-dark.png);
      background-position: center;
      padding: 0;
      margin: 0;
      position: relative;
      min-height: 100vh;
      padding-bottom: 385px;
      }
      .sk-cube-grid {
        width: 100px;
        height: 100px;
        position:fixed;
        margin-left: 47%;
        margin-top: 30vh;
        animation: moving1 linear;
        animation-duration: 0.5s;
        animation-delay: 1.5s;
        animation-fill-mode: both;
        opacity: 0.8;
      }
      @keyframes moving1 {
          0% {
          opacity: 0.8; z-index: 1;
          }
          100% {
          opacity: 0; z-index: -1;
        }
      }
      @keyframes moving {
          0% {
          opacity: 0; z-index: -1;
          }
          100% {
          opacity: 1; z-index: 1;
        }
      }
      .contentdiv{
        animation: moving linear;
        animation-duration: 1s;
        animation-delay: 2s;
        animation-fill-mode: both;
      }
      .sk-cube-grid .sk-cube {
        border-radius: 5px;
        width: 33%;
        height: 33%;
        float: left;
        -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                animation-iteration-count: 2;
      }
      .sk-cube-grid .sk-cube1 {
        -webkit-animation-delay: 0.2s;
                animation-delay: 0.2s;
                background-color: blue; }
      .sk-cube-grid .sk-cube2 {
        -webkit-animation-delay: 0.3s;
                animation-delay: 0.3s;
                background-color: lime;}
      .sk-cube-grid .sk-cube3 {
        -webkit-animation-delay: 0.4s;
                animation-delay: 0.4s;
              background-color: red;}
      .sk-cube-grid .sk-cube4 {
        -webkit-animation-delay: 0.1s;
                animation-delay: 0.1s;
              background-color: lightblue; }
      .sk-cube-grid .sk-cube5 {
        -webkit-animation-delay: 0.2s;
                animation-delay: 0.2s;
              background-color: purple; }
      .sk-cube-grid .sk-cube6 {
        -webkit-animation-delay: 0.3s;
                animation-delay: 0.3s;
              background-color: pink; }
      .sk-cube-grid .sk-cube7 {
        -webkit-animation-delay: 0s;
                animation-delay: 0s;
              background-color: yellow;}
      .sk-cube-grid .sk-cube8 {
        -webkit-animation-delay: 0.1s;
                animation-delay: 0.1s;
              background-color: orange; }
      .sk-cube-grid .sk-cube9 {
        -webkit-animation-delay: 0.2s;
                animation-delay: 0.2s;
              background-color: rgb(100,200,300);}

      @-webkit-keyframes sk-cubeGridScaleDelay {
        0%, 70%, 100% {
          -webkit-transform: scale3D(1, 1, 1);
                  transform: scale3D(1, 1, 1);
        } 35% {
          -webkit-transform: scale3D(0, 0, 1);
                  transform: scale3D(0, 0, 1);
        }
      }

      @keyframes sk-cubeGridScaleDelay {
        0%, 70%, 100% {
          -webkit-transform: scale3D(1, 1, 1);
                  transform: scale3D(1, 1, 1);
        } 35% {
          -webkit-transform: scale3D(0, 0, 1);
                  transform: scale3D(0, 0, 1);
        }

      }
      .row{
        width: 100%;
        padding: 1%;
        display: flex;
        flex-direction: row;
        justify-content: space-around;
      }
      @keyframes fadeapp {
        from {opacity: 0;}
        to {opacity: 1;}
      }
      .imga{
        opacity: 0;
        animation: fadeapp linear;
        animation-duration: 3s;
        animation-delay: 2.5s;
        animation-fill-mode: forwards;
      }
      .aligncenter {
        text-align: center;
      }
      .textdiv {
        margin: 20px;
        opacity: 0;
        animation: fadeapp linear;
        animation-duration: 3s;
        animation-delay: 2.5s;
        animation-fill-mode: forwards;
        background-color: rgba(8, 43, 112, 0.5);
        color: white;
        border: white solid 3px;
        border-radius: 5px;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        padding: 10px;
        height: 75%;
      }
      .footer11{
        padding: 10px;
        background-color: rgb(38, 45, 66);
        width: 100%;
        color: rgb(88, 89, 92);
        left: 0;
        bottom: 0;
        display: block;
        position: absolute;
        bottom: 0;
      }
      .logos{
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
      }
      .logo{
        margin-right: 20px;
      }
      .fq{
        background-image: url(https://i.pinimg.com/originals/1d/13/ea/1d13ead1c85dc568429e9672b48875e2.jpg);
        background-size: cover;
        opacity: 0.95;
        background-color: rgba(255, 0, 0, 0.5);
        padding: 30%;
        border-radius: 25%;
      }
    </style>

  </head>
  <body>
    <div class="sk-cube-grid">
      <div class="sk-cube sk-cube1"></div>
      <div class="sk-cube sk-cube2"></div>
      <div class="sk-cube sk-cube3"></div>
      <div class="sk-cube sk-cube4"></div>
      <div class="sk-cube sk-cube5"></div>
      <div class="sk-cube sk-cube6"></div>
      <div class="sk-cube sk-cube7"></div>
      <div class="sk-cube sk-cube8"></div>
      <div class="sk-cube sk-cube9"></div>
    </div>
    <div class="contentdiv">
      <?php include('navbar.php') ?>
      <div class="row" style="margin-top: 100px;">
        <div class="col-lg-2">

        </div>
        <div class="col-lg-8 col-md-12" style="text-align:center;">
          <h1 style="color: White;">Welcome to the AITU Mathematical Analysis Club WebPage!</h1>
          <img src="Logo..png" alt="" height="50%" class="imga">
          <br>
          <h3 style="color: White;"><span class="badge badge-primary">Access materials on our website to prepare for exams!</span></h3>
        </div>
        <div class="col-lg-2">

        </div>

      </div>
      <h1 style="color: white; margin-left: 50px;">Checkout our special courses:</h1>
      <div class="row aligncenter">
        <div class="col-lg-4 col-md-12 aligncenter">
          <img class="imga" src="https://www.mathsisfun.com/geometry/images/pythagoras-abc.svg" width="180px" height="25%" alt="">
          <div class="textdiv">
            <h3>Geometry</h3>
            <p>This course is designed to emphasize the study of the properties and applications of common geometric figures in two and three dimensions.  It includes the study of transformations and right triangle trigonometry.  Inductive and deductive thinking skills are used in problem solving situations, and applications to the real world are stressed.  It also emphasizes writing proofs to solve (prove) properties of geometric figures.  Students who complete Geometry should take Algebra II next.</p>
            <a href="geometry.php"><button type="button" class="btn btn-success">Acces this course</button></a>
          </div>
        </div>
        <div class="col-lg-4 col-md-12 aligncenter">
          <img class="imga" src="https://f0.pngfuel.com/png/709/466/number-theory-mathematics-saying-png-clip-art.png" alt="" width="180px" height="25%">
          <div class="textdiv">
            <h3>Modular Arithmetic <span class="badge badge-warning">New!</span> </h3>
            <p>Modular arithmetic is a system of arithmetic for integers, which considers the remainder. In modular arithmetic, numbers "wrap around" upon reaching a given fixed quantity (this given quantity is known as the modulus) to leave a remainder. Modular arithmetic is often tied to prime numbers, for instance, in Wilson's theorem, Lucas's theorem, and Hensel's lemma, and generally appears in fields like cryptography, computer science, and computer algebra.</p>
            <a href="modar.php"><button type="button" class="btn btn-success">Acces this course</button></a>
          </div>
        </div>
        <div class="col-lg-4 col-md-12 aligncenter">
          <img class="imga" src="https://cdn2.iconfinder.com/data/icons/flat-database/512/function-512.png" alt="" width="180px" height="25%">
          <div class="textdiv">
            <h3>Functions</h3>
            <p>Function, in mathematics, an expression, rule, or law that defines a relationship between one variable (the independent variable) and another variable (the dependent variable). Functions are ubiquitous in mathematics and are essential for formulating physical relationships in the sciences. This little course will provide students with basic knowledge about functions</p>
            <a href="funcs.php"><button type="button" class="btn btn-success">Acces this course</button></a>
          </div>
        </div>
      </div>
      <div class="textdiv" style="height: 100%; padding-bottom: 50px;">
        <div class="row">
          <img src="https://online-learning.harvard.edu/sites/default/files/styles/header/public/course/bigstock-123382757-1280x640.jpg?itok=73Kijdht" alt="">
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-12">
            <div class="textdiv aligncenter" style="background-color: rgba(100,200,100, 0.3); height: 100%;">
              <h3>Calculus I</h3>
              <a href="lim1.php"><button type="button" class="btn btn-light">Limits</button></a>
              <a href="diff1.php"><button type="button" class="btn btn-light">Differentiation</button></a>
              <hr>
              <a href="int1-1.php"><button type="button" class="btn btn-light">Simple Integration</button></a>
              <a href="int1-2.php"><button type="button" class="btn btn-light">Integration by substitution</button></a>
              <a href="int1-3.php"><button type="button" class="btn btn-light">Integration by parts</button></a>
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="textdiv aligncenter" style="background-color: rgba(100,200,100, 0.3); height: 100%;">
              <h3>Calculus II</h3>
              <a href="lim2.php"><button type="button" class="btn btn-light">Limits</button></a>
              <a href="diff2.php"><button type="button" class="btn btn-light">Differentiation</button></a>
              <a href="int2.php"><button type="button" class="btn btn-light">Double Integrals</button></a>

            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-10 col-md-10 fq aligncenter">
          <button type="button" name="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-success" style="width: 16em; height: 7em; border-radius: 5em;">Become a Partner</button>
        </div>

      </div>
        <?php include('footer.php') ?>
    </div>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Please provide your info:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="exampleFormControlInput1">Email address</label>
            <input type="email" class="form-control" id="email" placeholder="name@example.com">
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">Select your desired role</label>
            <select class="form-control" id="desiredrole">
              <option>Volunteer</option>
              <option>Teacher</option>
              <option>Lecturer</option>
              <option>Designer</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect2">Select your subject</label>
            <select multiple class="form-control" id="des_subjects">
              <option>Calculus</option>
              <option>Discrete mathematics</option>
              <option>Algorithms and Data Structures</option>
              <option>Web-development</option>
              <option>Economics</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlTextarea1">Describe yourself shortly</label>
            <textarea class="form-control" id="personal_desc" rows="3"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <script>
        function Thanks() {
          var email1 = String(document.getElementById("email").value);
          var role1 = String(document.getElementById("desiredrole").value);
          var subject1 = String(document.getElementById("des_subjects").value);
          var pdesc1 = String(document.getElementById("personal_desc").value);
          $.ajax({
               type: "POST",
               url: 'telegrambot.php',
               data:{email:email1, role: role1, subject:subject1, pdesc:pdesc1},
               success:function(html) {
                 alert("Thanks for feeling in the form! We will contact you shortly.");
               }
          });
        }
      </script>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="Thanks()">Send Info</button>

      </div>
    </div>
  </div>
</div>
  </body>
</html>
