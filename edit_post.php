<?php include('session.php') ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Edit your post</title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
<style media="screen">
body {
      background-color: black;
      font-family: Arial;
      display: block;
      background-image: url(repeated-square-dark.png);
      background-position: center;
      padding: 0;
      margin: 0;
      position: relative;
      min-height: 100vh;
      padding-bottom: 395px;
      }
      .sk-cube-grid {
        width: 100px;
        height: 100px;
        position:fixed;
        margin-left: 47%;
        margin-top: 30vh;
        animation: moving1 linear;
        animation-duration: 0.5s;
        animation-delay: 1.5s;
        animation-fill-mode: both;
        opacity: 0.8;
      }
      @keyframes moving1 {
          0% {
          opacity: 0.8; z-index: 1;
          }
          100% {
          opacity: 0; z-index: -1;
        }
      }
      @keyframes moving {
          0% {
          opacity: 0; z-index: -1;
          }
          100% {
          opacity: 1; z-index: 1;
        }
      }
      .contentdiv{
        animation: moving linear;
        animation-duration: 1s;
        animation-delay: 2s;
        animation-fill-mode: both;
      }
      .sk-cube-grid .sk-cube {
        border-radius: 5px;
        width: 33%;
        height: 33%;
        float: left;
        -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                animation-iteration-count: 2;
      }
      .sk-cube-grid .sk-cube1 {
        -webkit-animation-delay: 0.2s;
                animation-delay: 0.2s;
                background-color: blue; }
      .sk-cube-grid .sk-cube2 {
        -webkit-animation-delay: 0.3s;
                animation-delay: 0.3s;
                background-color: lime;}
      .sk-cube-grid .sk-cube3 {
        -webkit-animation-delay: 0.4s;
                animation-delay: 0.4s;
              background-color: red;}
      .sk-cube-grid .sk-cube4 {
        -webkit-animation-delay: 0.1s;
                animation-delay: 0.1s;
              background-color: lightblue; }
      .sk-cube-grid .sk-cube5 {
        -webkit-animation-delay: 0.2s;
                animation-delay: 0.2s;
              background-color: purple; }
      .sk-cube-grid .sk-cube6 {
        -webkit-animation-delay: 0.3s;
                animation-delay: 0.3s;
              background-color: pink; }
      .sk-cube-grid .sk-cube7 {
        -webkit-animation-delay: 0s;
                animation-delay: 0s;
              background-color: yellow;}
      .sk-cube-grid .sk-cube8 {
        -webkit-animation-delay: 0.1s;
                animation-delay: 0.1s;
              background-color: orange; }
      .sk-cube-grid .sk-cube9 {
        -webkit-animation-delay: 0.2s;
                animation-delay: 0.2s;
              background-color: rgb(100,200,300);}

      @-webkit-keyframes sk-cubeGridScaleDelay {
        0%, 70%, 100% {
          -webkit-transform: scale3D(1, 1, 1);
                  transform: scale3D(1, 1, 1);
        } 35% {
          -webkit-transform: scale3D(0, 0, 1);
                  transform: scale3D(0, 0, 1);
        }
      }

      @keyframes sk-cubeGridScaleDelay {
        0%, 70%, 100% {
          -webkit-transform: scale3D(1, 1, 1);
                  transform: scale3D(1, 1, 1);
        } 35% {
          -webkit-transform: scale3D(0, 0, 1);
                  transform: scale3D(0, 0, 1);
        }

      }
      .row{
        width: 100%;
        padding: 1%;
        display: flex;
        flex-direction: row;
        justify-content: space-around;
      }
      @keyframes fadeapp {
        from {opacity: 0;}
        to {opacity: 1;}
      }
      .imga{
        opacity: 0;
        animation: fadeapp linear;
        animation-duration: 3s;
        animation-delay: 2.5s;
        animation-fill-mode: forwards;
      }
      .aligncenter {
        text-align: center;
      }
      .textdiv {
        margin: 20px;
        opacity: 0;
        animation: fadeapp linear;
        animation-duration: 0.2s;
        animation-fill-mode: forwards;
        background-color: rgba(8, 43, 112, 0.5);
        color: white;
        border: white solid 3px;
        border-radius: 5px;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        padding: 60px;
        height: 70%;
      }
      .footer11{
        padding: 10px;
        background-color: rgb(38, 45, 66);
        width: 100%;
        color: rgb(88, 89, 92);
        position: absolute;
        bottom: 0;
      }
      .logos{
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
      }
      .logo{
        margin-right: 20px;
      }

   .edit{
     margin: 100px;
     opacity: 1;
     color: white;
     background-color: rgba(130,255,255,0.3);
     border: white solid 3px;
     border-radius: 5px;
     display: flex;
     text-align: center;
     flex-direction: column;
     justify-content: space-around;
     height: 500px;
     padding: 70px;
     height: 70%;
   }
</style>

  </head>
  <body>
    <?php include('navbar.php') ?>
  <?php include('footer.php'); ?>
  <div class="edit">
    <?php
    $sql_name = "SELECT * FROM posts";
    $res = mysqli_query($db, $sql_name);
    if($res == false){
      echo mysql_error($db);
    }
    $cout = mysqli_fetch_all($res, MYSQLI_ASSOC);
     ?>

    <?php
    $value = $_POST['id'];
    $value;
    $coutt = $cout[$value-1]["post_name"];
           $text = $cout[$value-1]["post_content"];
     ?>

     <form class=""  method="post">
       <div class="">
         <label for="name">Post name</label>
         <input class="form-control" style="margin: 10px;" type="text" name="name2" value="<?php echo $coutt; ?>">
       </div>
       <div class="">
         <h3>Text Content</h3>
         <textarea class="form-control" style="margin: 10px;" name="Text" rows="12" cols="100"><?php echo $text; ?></textarea>
         <input type="hidden" name="id" value="<?php echo $value; ?>">
       </div>
       <input class="btn btn-success" type="submit" name="" value="Edit post">
     </form>
     <?php
     if($_SERVER["REQUEST_METHOD"] == "POST") {
       if(isset($_POST["name2"])){
         $sql = "UPDATE posts SET post_name = '".$_POST['name2']."' Where post_id = '".$_POST['id']."'";
         $resultat = mysqli_query($db, $sql);
         if($resultat == false){
           echo mysqli_error($db);
         }else{
               echo "<script> location.href='forum.php'; </script>";
         }
       }
       if(isset($_POST["Text"])){
         $sql = "UPDATE posts SET post_content = '".$_POST['Text']."' Where post_id = '".$_POST['id']."'";
         $resultat = mysqli_query($db, $sql);
         if($resultat == false){
           echo mysqli_error($db);
         }else{
              echo "<script> location.href='forum.php'; </script>";

         }
       }
     }
      ?>
      <form class="" style="margin: 20px;" action="forum.php" method="post">
        <input class="btn btn-primary" type="submit" name="" value="Back to forum page">
      </form>

  </div>

  </body>
</html>
