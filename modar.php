<?php include('session.php') ?>
<?php include('navbar.php') ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Modular Arithmetic - AITU MathAnalysis</title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style media="screen">

          body {
          background-color: black;
          font-family: Arial;
          display: block;
          background-image: url(repeated-square-dark.png);
          background-position: center;
          padding: 0;
          margin: 0;
          }
          .sk-cube-grid {
            width: 100px;
            height: 100px;
            position:fixed;
            margin-left: 47%;
            margin-top: 30vh;
            animation: moving1 linear;
            animation-duration: 0.5s;
            animation-delay: 1.5s;
            animation-fill-mode: both;
            opacity: 0.8;
          }
          @keyframes moving1 {
              0% {
              opacity: 0.8; z-index: 1;
              }
              100% {
              opacity: 0; z-index: -1;
            }
          }
          @keyframes moving {
              0% {
              opacity: 0; z-index: -1;
              }
              100% {
              opacity: 1; z-index: 1;
            }
          }
          .contentdiv{
            animation: moving linear;
            animation-duration: 1s;
            animation-delay: 2s;
            animation-fill-mode: both;
          }
          .sk-cube-grid .sk-cube {
            border-radius: 5px;
            width: 33%;
            height: 33%;
            float: left;
            -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation-iteration-count: 2;
          }
          .sk-cube-grid .sk-cube1 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                    background-color: blue; }
          .sk-cube-grid .sk-cube2 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                    background-color: lime;}
          .sk-cube-grid .sk-cube3 {
            -webkit-animation-delay: 0.4s;
                    animation-delay: 0.4s;
                  background-color: red;}
          .sk-cube-grid .sk-cube4 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: lightblue; }
          .sk-cube-grid .sk-cube5 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: purple; }
          .sk-cube-grid .sk-cube6 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                  background-color: pink; }
          .sk-cube-grid .sk-cube7 {
            -webkit-animation-delay: 0s;
                    animation-delay: 0s;
                  background-color: yellow;}
          .sk-cube-grid .sk-cube8 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: orange; }
          .sk-cube-grid .sk-cube9 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: rgb(100,200,300);}

          @-webkit-keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }
          }

          @keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }

          }
          .row{
            width: 100%;
            padding: 1%;
            display: flex;
            flex-direction: row;
            justify-content: space-around;
          }
          @keyframes fadeapp {
            from {opacity: 0;}
            to {opacity: 1;}
          }
          .imga{
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 3s;
            animation-delay: 2.5s;
            animation-fill-mode: forwards;
          }
          .aligncenter {
            text-align: center;
          }
          .textdiv {
            margin: 20px;
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 2s;
            animation-fill-mode: forwards;
            background-color: rgba(8, 43, 112, 0.5);
            color: white;
            border: white solid 3px;
            border-radius: 5px;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            padding: 10px;
            height: 70%;
          }
          .footer11{
            padding: 10px;
            background-color: rgb(38, 45, 66);
            width: 100%;
            color: rgb(88, 89, 92);
            left: 0;
            bottom: 0;
            display: block;
          }
          .logos{
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
          }
          .logo{
            margin-right: 20px;
          }
    </style>

  </head>
  <body>

    <div class="textdiv" style="margin-top: 100px;">
      <div class="row">
        <div class="col-lg-5 col-md-12">
          <h3>Euclid's Algorithm</h3>
          <p>Given three integers <b><i>a, b, c</i></b>, can you write <b><i>c</i></b> in the form <br> <b><i>c=ax+by</i></b><br>for integers <i><b>x</b></i> and <i><b>y</b></i>? Can there be more than one solution? Can you find them all? Before answering this, let us answer a seemingly unrelated question:
How do you find the greatest common divisor (gcd) of two integers <i><b>a, b</b></i>?</p>
<br><p>Both of this problems could be solved using <b>Euclid's Algorithm.</b></p>
        </div>
        <div class="col-lg-5 col-md-12">
          <iframe width="100%" height="315" src="https://www.youtube.com/embed/cOwyHTiW4KE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      <div class="row">
        <script type="text/javascript">
          function gcd_a() {
            let a = document.getElementById('i1').value;
            let b = document.getElementById('i2').value;
            let S = gcd(a,b);
            document.getElementById('i3').value = S;
          }
          function gcd(a, b) {
            if (b == 0)
              return a;
            else
              return gcd(b, (a % b));
          }
        </script>
        <style media="screen">
          .m3{
            height:30px;
            width:150px;
            background-color: lightblue;
            border-radius:5px;
            margin: 20px;
          }
          .m3:hover{
            background-color: lime;
          }
          #i1{
            max-width: 200px;
          }
          #i2{
            max-width: 200px;
          }
          #i3{
            max-width: 200px;
          }
        </style>
        <div class="col-lg-5 col-md-9 textdiv" style ="background-color: rgba(120, 70, 120, 0.4);" >
          <h3>Euclid's Algorithm Calculator</h3>
          <label for="">Integer (a)</label>
          <input id="i1" type="number" name="" value="" >
          <label for="">Integer (b)</label>
          <input id="i2" type="number" name="" value="" >
          <input class="m3" id="3" type="button" onclick="gcd_a()" name="calculate" value="Getgcd">
          <label for="">GCD(a,b)</label>
          <input id="i3" type="number" name="" value="" >
        </div>
      </div>
    </div>
    <div class="textdiv" style="margin-top: 30px;">
      <div class="row">
        <div class="col-lg-5 col-md-12">
          <h3>Euler's Totient Function and Euler's Theorem</h3>
          <p>In number theory, Euler's totient function counts the positive integers up to a given integer <i><b>n</b></i> that are relatively prime to <i><b>n</b></i>. It is written using the Greek letter phi as <i><b>φ(n)</b></i> or <i><b>ϕ(n)</b></i>, and may also be called <b>Euler's phi function</b>. In other words, it is the number of integers <i><b>k</b></i> in the range <i><b>1 ≤ k ≤ n</b></i> for which the greatest common divisor <i><b>gcd(n, k)</b></i> is equal to 1.</p>
          <p> Euler's theorem (also known as the Fermat–Euler theorem or Euler's totient theorem) states that if <i><b>n</b></i> and <b><i>a</i></b> are coprime positive integers, then <br> <img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/2e818f3f88d3e71e569f171dd86f31e1903fdc55" style="background-color: rgba(255,255,255, 0.7);" alt=""> </p>
        </div>
        <div class="col-lg-5 col-md-12">
          <iframe width="100%" height="315" src="https://www.youtube.com/embed/oQ4ZmJF6s3E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      <div class="row">
        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
        <script type="text/javascript">
        'use strict';
        function GCD(a, b) {
            while (b != 0) {
                    var t = b;
                    b = a % b;
                    a = t;
            }
            return a;
        }

        function Phi(n) {
          var result;
          if(n>0){
            result = 1;
            for (var i = 2; i < n; i++) {
                    if (GCD(i, n) == 1) {
                            result+= 1;
                    }
            }
          }else {
            result = '(a) should be a natural number';
          }
            document.getElementById('j2').value = result;
        }

        </script>
        <style media="screen">
        #j1{
          max-width: 200px;
        }
        #j2{
          max-width: 200px;
        }
        .m4{
          width:150px;
          margin: 20px;
        }
        </style>
        <div class="col-lg-5 col-md-9 textdiv" style ="background-color: rgba(120, 70, 120, 0.4);">
          <h3>Euler's Totient Function Calculator</h3>
          <label for="">Natural number (n)</label>
          <input id="j1" type="number" name="" value="" >
          <input class="btn btn-success m4" type="button" onclick="Phi(document.getElementById('j1').value)" name="calculate" value="Calculate">
          <label for="">φ(n):</label>
          <input id="j2" type="number" name="" value="" >
        </div>
      </div>
    </div>


    <?php include('footer.php') ?>

  </body>
</html>
