<?php include('session.php') ?>
<?php include('navbar.php') ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Calculus I - Integration by substitution - AITU MathAnalysis</title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <style media="screen">

          body {
          background-color: black;
          font-family: Arial;
          display: block;
          background-image: url(repeated-square-dark.png);
          background-position: center;
          padding: 0;
          margin: 0;
          }
          .sk-cube-grid {
            width: 100px;
            height: 100px;
            position:fixed;
            margin-left: 47%;
            margin-top: 30vh;
            animation: moving1 linear;
            animation-duration: 0.5s;
            animation-delay: 1.5s;
            animation-fill-mode: both;
            opacity: 0.8;
          }
          @keyframes moving1 {
              0% {
              opacity: 0.8; z-index: 1;
              }
              100% {
              opacity: 0; z-index: -1;
            }
          }
          @keyframes moving {
              0% {
              opacity: 0; z-index: -1;
              }
              100% {
              opacity: 1; z-index: 1;
            }
          }
          .contentdiv{
            animation: moving linear;
            animation-duration: 1s;
            animation-delay: 2s;
            animation-fill-mode: both;
          }
          .sk-cube-grid .sk-cube {
            border-radius: 5px;
            width: 33%;
            height: 33%;
            float: left;
            -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation-iteration-count: 2;
          }
          .sk-cube-grid .sk-cube1 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                    background-color: blue; }
          .sk-cube-grid .sk-cube2 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                    background-color: lime;}
          .sk-cube-grid .sk-cube3 {
            -webkit-animation-delay: 0.4s;
                    animation-delay: 0.4s;
                  background-color: red;}
          .sk-cube-grid .sk-cube4 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: lightblue; }
          .sk-cube-grid .sk-cube5 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: purple; }
          .sk-cube-grid .sk-cube6 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                  background-color: pink; }
          .sk-cube-grid .sk-cube7 {
            -webkit-animation-delay: 0s;
                    animation-delay: 0s;
                  background-color: yellow;}
          .sk-cube-grid .sk-cube8 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: orange; }
          .sk-cube-grid .sk-cube9 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: rgb(100,200,300);}

          @-webkit-keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }
          }

          @keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }

          }
          .row{
            width: 100%;
            padding: 1%;
            display: flex;
            flex-direction: row;
            justify-content: space-around;
          }
          @keyframes fadeapp {
            from {opacity: 0;}
            to {opacity: 1;}
          }
          .imga{
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 3s;
            animation-delay: 2.5s;
            animation-fill-mode: forwards;
          }
          .aligncenter {
            text-align: center;
          }
          .textdiv {
            margin: 20px;
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 1s;
            animation-fill-mode: forwards;
            background-color: rgba(8, 43, 112, 0.5);
            color: white;
            border: white solid 3px;
            border-radius: 5px;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            padding: 10px;
            height: 70%;
          }
          .footer11{
            padding: 10px;
            background-color: rgb(38, 45, 66);
            width: 100%;
            color: rgb(88, 89, 92);
          }
          .logos{
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
          }
          .logo{
            margin-right: 20px;
          }
    </style>

  </head>
  <body>

    <div class="textdiv" style="margin-top: 100px;">
      <h1 class="aligncenter">Integration by Substitution</h1>
      <p>All of the integrals we’ve done to this point have required that we just had an \(x\), or a \(t\), or a \(w\), <em>etc</em>. and not more complicated terms such as,</p>

    \[\begin{array}{c}\displaystyle \int{{18{x^2}\,\sqrt[4]{{6{x^3} + 5}}\,dx}}\hspace{0.5in}\int{{\frac{{2{t^3} + 1}}{{{{\left( {{t^4} + 2t} \right)}^3}}}\,dt}}\hspace{0.25in}\\ \displaystyle  \int{{\left( {1 - \frac{1}{w}} \right)\cos \left( {w - \ln w} \right)\,dw}}\hspace{0.5in}\,\,\int{{\left( {8y - 1} \right){{\bf{e}}^{4{y^2} - y}}\,dy}}\end{array}\]

<p>All of these look considerably more difficult than the first set.  However, they aren’t too bad once you see how to do them.  Let’s start with the first one.</p>

    \[\int{{18{x^2}\,\sqrt[4]{{6{x^3} + 5}}\,dx}}\]

<p>In this case let’s notice that if we let</p>

    \[u = 6{x^3} + 5\]
<p>and we compute the differential (you remember how to compute these right?) for this we get,</p>

    \[du = 18{x^2}dx\]

<p>Now, let’s go back to our integral and notice that we can eliminate every \(x\) that exists in the integral and write the integral completely in terms of \(u\) using both the definition of \(u\) and its differential.</p>

  \[\begin{align*}\int{{18{x^2}\,\sqrt[4]{{6{x^3} + 5}}\,dx}} & = \int{{\,{{\left( {6{x^3} + 5} \right)}^{\frac{1}{4}}}\,\left( {18{x^2}dx} \right)}}\\ &amp;  = \int{{{u^{\frac{1}{4}}}\,du}}\end{align*}\]

<p>In the process of doing this we’ve taken an integral that looked very difficult and with a quick substitution we were able to rewrite the integral into a very simple integral that we can do.</p>

<p>Evaluating the integral gives,</p>

    \[\int{{18{x^2}\,\sqrt[4]{{6{x^3} + 5}}\,dx}} = \int{{{u^{\frac{1}{4}}}\,du}} = \frac{4}{5}{u^{\frac{5}{4}}} + c = \frac{4}{5}{\left( {6{x^3} + 5} \right)^{\frac{5}{4}}} + c\]

<p>As always, we can check our answer with a quick derivative if we’d like to and don’t forget to “back substitute” and get the integral back into terms of the original variable.</p>

<p>What we’ve done in the work above is called the <strong>Substitution Rule</strong>.  Here is the substitution rule in general.</p>
  <div class="textdiv col-lg-8 col-md-12" style="margin: auto; background-color: rgba(170,120,140,0.3);">
    <h4>Substitution Rule</h4>
    <div class="fact">
      \[\int{{f\left( {g\left( x \right)} \right)\,g'\left( x \right)\,dx}} = \int{{f\left( u \right)\,du}},\hspace{0.25in}{\mbox{where, }}u = g\left( x \right)\]
    </div>
  </div>
    </div>
    <div class="textdiv">
      <div class="col-12-lg col-md-12 aligncenter">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/AWfTs4S648Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <?php include('footer.php') ?>

  </body>
</html>
