-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июн 20 2020 г., 17:24
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `final`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `Id` int(100) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_seen` varchar(20) NOT NULL,
  `description` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`Id`, `username`, `password`, `last_seen`, `description`, `status`, `image`) VALUES
(1, 'Zufchan', '$2y$10$06veHLJyK5mdrA9ckUmzOuwdYvnw/wcs1mmzy3doWk/RbnjnqVY4O', '20.06.2020 17:23:01', '$$i\\frac{hate}{my}life$$', 'admin', 'images/desktop-source-code-wallpaper-by-computer-language-with-coding-programming_33771-599.jpg'),
(2, 'Kassym', '$2y$10$06veHLJyK5mdrA9ckUmzOuwdYvnw/wcs1mmzy3doWk/RbnjnqVY4O', '20.06.2020 17:23:07', 'Hello, my name is Kassym', 'member', 'images/photo_2020-06-20_00-32-25.jpg'),
(3, 'Shyngys', '$2y$10$Z6c2GoqrgHe7R9WbFP5FpuAjbFYl.IV0VOx/bm5lYEzL1A6.UsGHG', '20.06.2020 17:23:16', '', 'member', '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
