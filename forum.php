<?php include('session.php') ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Forum - AITU MathAnalysis</title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
<style media="screen">
   <?php include('style.css') ?>
   .diver{
     width: 100%;
     color: white;
     border-radius: 20px;
     display: flex;
     flex-direction: column;
     box-shadow: 0 2px 20px tan, 0 3px 10px grey;
   }
</style>
  </head>
  <body>
    <?php include('navbar.php') ?>

      <?php include('footer.php'); ?>
    <div style="position: fixed; bottom: 70px; left: 50px; z-index: 2">
      <form class=""  action="createpost.php" method="post">
        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
        <input type="submit" class="btn btn-danger"  name="" value="Create new post">
      </form>
    </div>
    <div style="margin-top:100px;">

    </div>
        <?php
            $sql_name = "SELECT * FROM posts";
             $res = mysqli_query($db, $sql_name);
             if($res == false){
               echo mysql_error($db);
             }
             $cout = mysqli_fetch_all($res, MYSQLI_ASSOC);
          ?>
          <?php  foreach ($cout as $posts): ?>
              <div class="row">
                <div class="col-lg-8">
            <?php $tid =  $posts['post_id'];?>
            <?php
            if($posts['user_id'] == $user_id || $user_status == "admin"){
              $access = "submit";
            }else{
              $access = "hidden";
            } ?>
            <?php
            $qr = mysqli_query($db, "Select username from users where Id = '".$posts['user_id']."'");
            $qres = mysqli_fetch_array($qr, MYSQLI_ASSOC);
            $author = $qres["username"];
             ?>
            <div class="diver" style="background-color: rgba(<?php echo 100 ?>, <?php echo 100; ?>, <?php echo 190; ?>, 0.7); padding:30px; " id="post<?php echo $tid; ?>">

              <h1 class="aligncenter"><?php echo $posts['post_name'] ?></h1>
              <br>
              <div class="card">
                <div class="card-body">
                  <p style="font-size: 20px; color: black;"><?php echo $posts['post_content'] ?></p>
                </div>

              </div>
                <form class="" action="profile.php" method="post">
              <p style="color: #95F2CA; font-size: 15px; margin: 1em;"><b>Author:
                  <input type="submit" style="color: #95F16A; background-color: rgba(70,200,150,0.5); border-radius: 10px; border: 0px;" name="profile_name" value="<?php echo $author ?>">
                 <br></b><i style="color: lightblue;">Posted on: <?php echo $posts['date'] ;?></i></p>
              </form>
              <div class="" style="display:flex; flex-direction:row; justify-content: space-between;">
                <form class="" action="edit_post.php" method="post">
                  <input type="hidden" name="id" value="<?php echo $tid; ?>">
                  <input style="margin: 10px;" class="aligncenter btn btn-primary" type="<?php echo $access; ?>" value="Edit Post">
                </form>
                <script type="text/javascript">
                function deletepost(del_id) {
                  var post = "#post"+del_id;
                    $.ajax({
                         type: "POST",
                         url: 'deletepost.php',
                         data:{action:'deletepost', idpost: del_id},
                         success:function(html) {
                           $(post).slideToggle();
                         }

                    });
                  }
                  function deleting(del_id) {
                    var result = confirm("Want to delete?");
                    if (result) {
                      deletepost(del_id);
                    }
                  }


                </script>
                  <input class="btn btn-danger" style="margin:10px;" type="<?php echo $access; ?>" onclick="deleting(<?php echo $tid; ?>)" id = "delete" type="button" name="" value="Delete post">

              </div>

            </div>
      </div>
    </div>
      <?php endforeach; ?>

  </body>
</html>
