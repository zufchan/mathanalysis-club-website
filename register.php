<?php
   include('Config.php');
   session_start();
   $error = "";
   $sucs = "";
// Check connection
if (!$db) {
  die("Connection failed: " . mysqli_connect_error());
}

if($_SERVER["REQUEST_METHOD"] == "POST") {
  if(isset($_POST['username']) && isset($_POST['password'])){
    $myusername = $_POST['username'];
    $mypassword = $_POST['password'];
    $temp = $_POST['reppasw'];
    $sql2 = "Select Id from users where username = '".$myusername."'";
    $result1 = mysqli_query($db,$sql2);
    $count3 = mysqli_num_rows($result1);
    if ($count3 == 0) {
      if ($temp == $mypassword) {
        $sql1 = "Select Id from users";
        $result = mysqli_query($db,$sql1);
        $count = mysqli_num_rows($result);
        $Id = $count+1;
        $hashpassword = password_hash($mypassword, PASSWORD_DEFAULT);
        $sql = "INSERT INTO users (Id, username, password, status)
        VALUES ('".$Id."', '".$myusername."', '".$hashpassword."', 'member')";

        if (mysqli_query($db, $sql)) {
          $sucs = "New user was created successfully! Return to login page.";
        } else {
          $error = "Unexpected error, contact administrator.";
        }
      }else {
        $error = "Passwords are not the same!";
      }

    }else {
      $error = "Such username already exists!";
    }
  }else{
    $error = "All fields Must be filled!";
  }
}
mysqli_close($db);
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Functions - AITU MathAnalysis</title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <style media="screen">

          body {
          background-color: black;
          font-family: Arial;
          display: flex;
          flex-direction: column;
          justify-content: center;
          background-image: url(repeated-square-dark.png);
          background-position: center;
          padding: 0;
          margin: 0;
          }
          .sk-cube-grid {
            width: 100px;
            height: 100px;
            position:fixed;
            margin-left: 47%;
            margin-top: 30vh;
            animation: moving1 linear;
            animation-duration: 0.5s;
            animation-delay: 1.5s;
            animation-fill-mode: both;
            opacity: 0.8;
          }
          @keyframes moving1 {
              0% {
              opacity: 0.8; z-index: 1;
              }
              100% {
              opacity: 0; z-index: -1;
            }
          }
          @keyframes moving {
              0% {
              opacity: 0; z-index: -1;
              }
              100% {
              opacity: 1; z-index: 1;
            }
          }
          .contentdiv{
            animation: moving linear;
            animation-duration: 1s;
            animation-delay: 2s;
            animation-fill-mode: both;
          }
          .sk-cube-grid .sk-cube {
            border-radius: 5px;
            width: 33%;
            height: 33%;
            float: left;
            -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation-iteration-count: 2;
          }
          .sk-cube-grid .sk-cube1 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                    background-color: blue; }
          .sk-cube-grid .sk-cube2 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                    background-color: lime;}
          .sk-cube-grid .sk-cube3 {
            -webkit-animation-delay: 0.4s;
                    animation-delay: 0.4s;
                  background-color: red;}
          .sk-cube-grid .sk-cube4 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: lightblue; }
          .sk-cube-grid .sk-cube5 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: purple; }
          .sk-cube-grid .sk-cube6 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                  background-color: pink; }
          .sk-cube-grid .sk-cube7 {
            -webkit-animation-delay: 0s;
                    animation-delay: 0s;
                  background-color: yellow;}
          .sk-cube-grid .sk-cube8 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: orange; }
          .sk-cube-grid .sk-cube9 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: rgb(100,200,300);}

          @-webkit-keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }
          }

          @keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }

          }
          .row{
            width: 100%;
            padding: 1%;
            display: flex;
            flex-direction: row;
            justify-content: space-around;
          }
          @keyframes fadeapp {
            from {opacity: 0;}
            to {opacity: 1;}
          }
          .imga{
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 3s;
            animation-delay: 2.5s;
            animation-fill-mode: forwards;
          }
          .aligncenter {
            text-align: center;
          }
          .textdiv {
            margin: 20px;
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 1s;
            animation-fill-mode: forwards;
            background-color: rgba(8, 43, 112, 0.5);
            color: white;
            border: white solid 3px;
            border-radius: 5px;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            padding: 10px;
            height: 70%;
          }
          .footer11{
            padding: 10px;
            background-color: rgb(38, 45, 66);
            width: 100%;
            color: rgb(88, 89, 92);
          }
          .logos{
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
          }
          .logo{
            margin-right: 20px;
          }
    </style>

  </head>
  <body>
    <div class="container">
        <div class="row justify-content-center">
        <div class="col-md-6">
        <div class="card">
        <header class="card-header">
        	<a href="loginpage.php" class="float-right btn btn-outline-primary mt-1">Log in</a>
        	<h4 class="card-title mt-2">Sign up</h4>
        </header>
        <article class="card-body">
        <form method="post">
        	<div class="form-row">
        		<div class="col form-group">
        			<label>Username</label>
        		  	<input type="text" class="form-control" name="username" placeholder="">
        		</div> <!-- form-group end.// -->
        		<div class="col form-group">
        			<label>Password</label>
        		  	<input type="password" class="form-control" name="password" placeholder="">
        		</div> <!-- form-group end.// -->
        	</div> <!-- form-row end.// -->
        	<div class="form-group">
        		<label>Repeate password</label>
        	    <input class="form-control" type="password" name="reppasw">
        	</div> <!-- form-group end.// -->
          <div class="form-group">
            <div style = "font-size:1em; color:#cc0000; margin-top:10px"><?php echo $error; ?></div>
            <div style = "font-size:1em; color:green; margin-top:10px"><?php echo $sucs; ?></div>
          </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Register  </button>
            </div> <!-- form-group// -->
            <small class="text-muted">By clicking the 'Sign Up' button, you confirm that you accept our <br> Terms of use and Privacy Policy.</small>
        </form>
        </article> <!-- card-body end .// -->
        <div class="border-top card-body text-center">Have an account? <a href="loginpage.php">Log In</a></div>
        </div> <!-- card.// -->
        </div> <!-- col.//-->



        </div>
  </body>
</html>
