<?php include('session.php') ?>
<?php include('navbar.php') ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Calculus I - Simple Integration - AITU MathAnalysis</title>
    
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style media="screen">

          body {
          background-color: black;
          font-family: Arial;
          display: block;
          background-image: url(repeated-square-dark.png);
          background-position: center;
          padding: 0;
          margin: 0;
          }
          .sk-cube-grid {
            width: 100px;
            height: 100px;
            position:fixed;
            margin-left: 47%;
            margin-top: 30vh;
            animation: moving1 linear;
            animation-duration: 0.5s;
            animation-delay: 1.5s;
            animation-fill-mode: both;
            opacity: 0.8;
          }
          @keyframes moving1 {
              0% {
              opacity: 0.8; z-index: 1;
              }
              100% {
              opacity: 0; z-index: -1;
            }
          }
          @keyframes moving {
              0% {
              opacity: 0; z-index: -1;
              }
              100% {
              opacity: 1; z-index: 1;
            }
          }
          .contentdiv{
            animation: moving linear;
            animation-duration: 1s;
            animation-delay: 2s;
            animation-fill-mode: both;
          }
          .sk-cube-grid .sk-cube {
            border-radius: 5px;
            width: 33%;
            height: 33%;
            float: left;
            -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation-iteration-count: 2;
          }
          .sk-cube-grid .sk-cube1 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                    background-color: blue; }
          .sk-cube-grid .sk-cube2 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                    background-color: lime;}
          .sk-cube-grid .sk-cube3 {
            -webkit-animation-delay: 0.4s;
                    animation-delay: 0.4s;
                  background-color: red;}
          .sk-cube-grid .sk-cube4 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: lightblue; }
          .sk-cube-grid .sk-cube5 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: purple; }
          .sk-cube-grid .sk-cube6 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                  background-color: pink; }
          .sk-cube-grid .sk-cube7 {
            -webkit-animation-delay: 0s;
                    animation-delay: 0s;
                  background-color: yellow;}
          .sk-cube-grid .sk-cube8 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: orange; }
          .sk-cube-grid .sk-cube9 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: rgb(100,200,300);}

          @-webkit-keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }
          }

          @keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }

          }
          .row{
            width: 100%;
            padding: 1%;
            display: flex;
            flex-direction: row;
            justify-content: space-around;
          }
          @keyframes fadeapp {
            from {opacity: 0;}
            to {opacity: 1;}
          }
          .imga{
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 3s;
            animation-delay: 2.5s;
            animation-fill-mode: forwards;
          }
          .aligncenter {
            text-align: center;
          }
          .textdiv {
            margin: 20px;
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 1s;
            animation-fill-mode: forwards;
            background-color: rgba(8, 43, 112, 0.5);
            color: white;
            border: white solid 3px;
            border-radius: 5px;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            padding: 10px;
            height: 70%;
          }
          .footer11{
            padding: 10px;
            background-color: rgb(38, 45, 66);
            width: 100%;
            color: rgb(88, 89, 92);
          }
          .logos{
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
          }
          .logo{
            margin-right: 20px;
          }
    </style>

  </head>
  <body>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

    <h1 class="aligncenter" style="color:white; margin-top:100px;">Simple Integrals</h1>
    <div class="textdiv">
      <h4>Definitions</h4>
        <p>Given a function, \(f\left( x \right)\), an <strong>anti-derivative</strong> of \(f\left( x \right)\) is any function \(F\left( x \right)\) such that</p>

          \[F'\left( x \right) = f\left( x \right)\]

        <p>If \(F\left( x \right)\) is any anti-derivative of \(f\left( x \right)\) then the most general anti-derivative of \(f\left( x \right)\) is called an <strong>indefinite integral</strong> and denoted,</p>


        \[\int{{f\left( x \right)\,dx}} = F\left( x \right) + c,\hspace{0.25in}\,\,\,\,c{\mbox{ is any constant}}\]

        <p>In this definition the \(\int{{}}\)is called the <strong>integral symbol</strong>,  \(f\left( x \right)\) is called the <strong>integrand</strong>, \(x\) is called the <strong>integration variable</strong> and the “\(c\)” is called the <strong>constant of integration</strong>.</p>
    </div>
    <div class="textdiv">
      <p>The first integral that we’ll look at is the integral of a power of \(x\).</p>


        \[\int{{{x^n}\,dx}} = \frac{{{x^{n + 1}}}}{{n + 1}} + c,\,\,\,\,\,n \ne  - 1\]

    <p>The general rule when integrating a power of \(x\) we add one onto the exponent and then divide by the new exponent.  It is clear (hopefully) that we will need to avoid \(n =  - 1\) in this formula.  If we allow \(n =  - 1\) in this formula we will end up with division by zero.  We will take care of this case in a bit.</p>

    <p>Next is one of the easier integrals but always seems to cause problems for people.</p>


        \[\int{{k\,dx}} = kx + c,\hspace{0.25in}\,\,\,\,c{\mbox{ and }}k{\mbox{ are constants}}\]

    <p>If you remember that all we’re asking is what did we differentiate to get the integrand this is pretty simple, but it does seem to cause problems on occasion.</p>

    <p>Let’s now take a look at the trig functions.</p>


      \[\begin{array}{ll}\int{{\sin x\,dx}} =  - \cos x + c \hspace{0.75in} & \int{{\cos x\,dx}} = \sin x + c\\ \int{{{{\sec }^2}x\,dx}} = \tan x + c \hspace{0.75in} & \int{{\sec x\tan x\,dx}} = \sec x + c\\ \int{{{{\csc }^2}x\,dx}} =  - \cot x + c \hspace{0.75in} & \int{{\csc x\cot x\,dx}} =  - \csc x + c\end{array}\]

    <p>Notice that we only integrated two of the six trig functions here.  The remaining four integrals are really integrals that give the remaining four trig functions.  Also, be careful with signs here.  It is easy to get the signs for derivatives and integrals mixed up.  Again, remember that we’re asking what function we differentiated to get the integrand.</p>

    <p>We will be able to integrate the remaining four trig functions in a couple of sections, but they all require the Substitution Rule.</p>

    <p>Now, let’s take care of exponential and logarithm functions.</p>


      \[\int{{{{\bf{e}}^x}\,dx}} = {{\bf{e}}^x} + c\hspace{0.25in}\,\hspace{0.25in}\,\,\,\,\int{{{a^x}\,dx}} = \frac{{{a^x}}}{{\ln a}} + c\hspace{0.5in}\int{{\frac{1}{x}\,dx}} = \int{{{x^{ - 1}}\,dx}} = \ln \left| x \right| + c\]

    <p>Integrating logarithms requires a topic that is usually taught in Calculus II and so we won’t be integrating a logarithm in this class.  Also note the third integrand can be written in a couple of ways and don’t forget the absolute value bars in the \(x\) in the answer to the third integral.</p>

    <p>Finally, let’s take care of the inverse trig and hyperbolic functions.</p>


        \[\begin{array}{ll}\displaystyle \int{{\frac{1}{{{x^2} + 1}}\,dx}} = {\tan ^{ - 1}}x + c\hspace{0.75in} & \displaystyle \int{{\frac{1}{{\sqrt {1 - {x^2}} }}\,dx}} = {\sin ^{ - 1}}x + c\\  \int{{\sinh x\,dx}} = \cosh x + c\hspace{0.75in} & \int{{\cosh x\,dx}} = \sinh x + c\\ \int{{{{{\mathop{\rm sech}\nolimits} }^2}x\,dx}} = \tanh x + c\hspace{0.75in}& \int{{{\mathop{\rm sech}\nolimits} x\tanh x\,dx}} =  - {\mathop{\rm sech}\nolimits} x + c\\ \int{{{{{\mathop{\rm csch}\nolimits} }^2}x\,dx}} =  - \coth x + c\hspace{0.75in} & \int{{{\mathop{\rm csch}\nolimits} x\coth x\,dx}} =  - {\mathop{\rm csch}\nolimits} x + c\end{array}\]

    <p>As with logarithms integrating inverse trig functions requires a topic usually taught in Calculus II and so we won’t be integrating them in this class.  There is also a different answer for the second integral above.  Recalling that since all we are asking here is what function did we differentiate to get the integrand the second integral could also be,</p>

        \[\int{{\frac{1}{{\sqrt {1 - {x^2}} }}\,dx}} =  - {\cos ^{ - 1}}x + c\]

    <p>Traditionally we use the first form of this integral.</p>

    <p>Okay, now that we’ve got most of the basic integrals out of the way let’s do some indefinite integrals.  In all of these problems remember that we can always check our answer by differentiating and making sure that we get the integrand.</p>
    </div>
    <div class="textdiv">
      <div class="row aligncenter">
        <div class="col-lg-5 col-md-12">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/rfG8ce4nNh0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-lg-5 col-md-12">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/FnJqaIESC2s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
    <?php include('footer.php') ?>

  </body>
</html>
