<?php include('session.php') ?>
<?php
if(isset($_POST['profile_name'])){

  $profile_name = $_POST['profile_name'];
}else{
  $profile_name = $login_session;
}
$profile_qry = mysqli_query($db, "select * from users where username = '".$profile_name."'");
$profile_res = mysqli_fetch_all($profile_qry, MYSQLI_ASSOC);
$profile_status = $profile_res[0]['status'];

$temp_last_seen = explode(" ", $profile_res[0]['last_seen']);
$temp_time = explode(":", $temp_last_seen[1]);
$profile_last_seen = $temp_last_seen[0].", at ".((intval($temp_time[0])+4)%24).":".$temp_time[1];
$profile_desc = $profile_res[0]['description'];
$profile_img = $profile_res[0]['image'];
if($profile_img == ""){
  $profile_img = "images/default.jpg";
}
$editting = "none";
if($profile_name == $login_session){
  $editting = "block";
}
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="stuff.ico">
    <title>Profile - <?php echo $profile_name ?></title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
  </head>
  <body>
    <?php include('navbar.php') ?>
    <div class="row">
      <div class="col-lg-5" style="margin-top: 100px;">
        <div class="card" style="width:100%">
          <img class="card-img-top" src="<?php echo $profile_img; ?>" alt="Card image">
          <div class="card-body">
            <h4 class="card-title"><?php echo $profile_name." " ?><span class="badge badge-success" ><?php echo $profile_status ?></span></h4>
            <p class="card-text"><?php echo $profile_desc ?></p>

            <p class="card-text">Last seen: <?php echo $profile_last_seen ?></p>
            <form class="" style="display: <?php echo $editting ?>" action="edit_profile.php" method="post">
              <button type="submit" class="btn btn-primary" name="button">Edit description</button>
            </form>
            <br>
            <hr>
            <form class=""  style="display: <?php echo $editting ?>" action="update_image.php" method="post" enctype="multipart/form-data">
              <input type="file" class="btn btn-light" name="img" value="">
              <button type="submit" class="btn btn-success" name="submit" style="float: right;">Update image</button>
            </form>

            </div>
          </div>
        </div>
      </div>
    </div>

    <?php include('footer.php') ?>
  </body>
</html>
