<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style media="screen">
    .row{
      width: 100%;
      padding: 1%;
      display: flex;
      flex-direction: row;
      justify-content: space-around;
    }
    @keyframes fadeapp {
      from {opacity: 0;}
      to {opacity: 1;}
    }
    .imga{
      opacity: 0;
      animation: fadeapp linear;
      animation-duration: 3s;
      animation-delay: 2.5s;
      animation-fill-mode: forwards;
    }
    .aligncenter {
      text-align: center;
    }
    .textdiv {
      margin: 20px;
      opacity: 0;
      animation: fadeapp linear;
      animation-duration: 3s;
      animation-delay: 2.5s;
      animation-fill-mode: forwards;
      background-color: rgba(8, 43, 112, 0.5);
      color: white;
      border: white solid 3px;
      border-radius: 5px;
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      padding: 10px;
      height: 75%;
    }
    .footer11{
      padding: 10px;
      background-color: rgb(38, 45, 66);
      width: 100%;
      color: rgb(88, 89, 92);
      left: 0;
      bottom: 0;
      display: block;
    }
    .logos{
      display: flex;
      flex-direction: row;
      justify-content: flex-start;
    }
    .logo{
      margin-right: 20px;
    }
    .fq{
      background-image: url(https://i.pinimg.com/originals/1d/13/ea/1d13ead1c85dc568429e9672b48875e2.jpg);
      background-size: cover;
      opacity: 0.95;
      background-color: rgba(255, 0, 0, 0.5);
      padding: 30%;
      border-radius: 25%;
    }
    </style>
  </head>
  <body>
    <div class="footer11">
      <div class="row">
        <div class="col-lg-2">

        </div>
        <div class="col-lg-4 col-md-12">
          <br>
          <h4>About us:</h4>
          <br>
          <p>An educational club in Astana IT University which provides math based lectures and face-to-face lessons to students from another students or from teahcers.</p>

        </div>
        <div class="col-lg-4 col-md-12">
          <h4>Contact us:</h4>
          <p>+7 777 009 0049</p>
          <p>Inst: @zufchan</p>
          <p>Logo design: #marshallrk</p>

          <div class="logos">
            <img src="Logo..png" alt="" width="60px;" class="logo">
            <img src="https://astanait.edu.kz/wp-content/uploads/2020/05/aitu-logo-white-2-300x154.png" alt="" width="120px;" class="logo">
          </div>
        </div>
        <div class="col-lg-2">

        </div>
      </div>
    </div>
  </body>
</html>
