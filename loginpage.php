<?php
   include("Config.php");
   session_start();
   $error = "";
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form
      if(isset($_POST['username']) && isset($_POST['password'])){
      $myusername = $_POST['username'];
      $mypassword = $_POST['password'];

      $sql = "SELECT Id, password FROM users WHERE username = '".$myusername."'";
      $result = mysqli_query($db,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $count = mysqli_num_rows($result);
      $pasword = $row['password'];

      // If result matched $myusername and $mypassword, table row must be 1 row

      if($count == 1 && password_verify($mypassword, $pasword)) {
         $_SESSION['login_user'] = $myusername;

         header("location: mainpage.php");
      }else {
         $error = "Your Login Name or Password is invalid";
      }
   }else {
     alert("Both password and username should be set");
   }
 }
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Login</title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

      <style>
          body {
          background-color: black;
          font-family: Georgia;
          display: block;
          background-position: center;
          }
          .grid {
          display: grid;
          grid-template-columns: 1fr 1fr 1fr;
          grid-template-rows: 1fr 1fr 1fr;
          background-color: rgba(110, 107, 255, 0.8);
          padding: 10px;
          grid-gap: 7px;
          justify-content: start;
          max-width: 800px;
          margin: auto;
          border-radius: 5px;
          }

          .grid > div {
          padding: 10px;
          }
          .div1 {
          grid-column-start: 1;
          grid-column-end: 4;
          grid-row-start: 1;
          grid-row-end: 2;
          font-weight: bolder;
          font-size: 30px;
          background-color: #435399;
          border-radius: 5px;
          }
          .div2 {
          grid-column-start: 1;
          grid-column-end: 4;
          grid-row-start: 2;
          grid-row-end: 12;
          background-color: rgb(100, 160, 220);
          border-radius: 5px;
          font-size: 25px;
          }
          .div3 {
          grid-column-start: 1;
          grid-column-end: 4;
          grid-row-start: 12;
          grid-row-end: 14;
          display: flex;
          flex-direction: row;
          justify-content: space-between;
          }
          .unam {
          padding: 10px 0 10px 10px;
          width: 60%;
          border: 1px solid lightblue;
          border-radius: 5px;
          margin: 0;
          }
          .password {
          padding: 10px 0 10px 10px;
          width: 60%;
          border: 1px solid lightblue;
          border-radius: 5px;
          margin-top: 10px;
          margin-bottom: 3%;
          }
          .btn {
          padding: 0.3% 10%;
          border-radius: 5px;
          margin-left: 35%;
          background-color: rgb(120, 133, 240);
          border: 4px solid rgb(90, 133, 230);
          color: white;
          font-size: 20px;
          }
          .btn:hover {
            background-color: rgb(170,100,250);
            border: 4px solid rgb(180,100,200);
          }
          .frg {
          float: right;
          margin-top: 1.5%;
          font-size: 13.5px;
          color: #3a035c;
          }
          .newacc {

          display: inline;
          float: left;
          margin-top: 7%;
          }
          .help {
          display: inline;
          margin-top: 7%;
          }
        .imga{
          position: absolute;
          opacity: 0.9;
          top: 30vh;
          margin: auto;
        }
        #particles-js{
          position: relative;
          width: 100%;

        }
      </style>
    </head>
  <body>
    <div id="particles-js" style="display: flex; flex-direction: column; justify-content: space-around; width: 100%; ">
      <div class="grid" style="position: absolute; position: absolute; left: 50%; margin-left: -400px;">
        <div class="div1">Login Here</div>
        <div class="div2">
          <form action="" method="post">
            <label style="margin-right: 20%;">Username</label>
            <input class="unam" type="text" name="username">
            <label style="margin-right: 20.5%;">Password</label>
            <input class="password" type="password" name="password">
            <input class="btn" type="submit" value="Login">
          </form>
         <div style = "font-size:0.5em; color:#cc0000; margin-top:10px"><?php echo $error; ?></div>
        </div>
            <div class="div3">
              <div class="" style="background-color: lightblue; border: solid white 1px; padding-bottom: 5px; padding-top: 5px;padding-left: 15px; padding-right: 15px; height: 70px; border-radius: 5px;" >
                <p class="newacc">Create a new account <a href="register.php" style="color: purple;">here.</a> </p>
              </div>
              <img src="Logo..png" height="100px" alt="" style="opacity: 0.9; left: 100px;">
              <p class="help">If you need any other help, please contact <br> @xever1 at telegram.</p>
            </div>
          </div>
    </div>
    <script src="particles.js"></script>
    <script src="app.js"></script>
  </body>
</html>
