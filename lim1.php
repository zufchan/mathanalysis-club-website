<?php include('session.php') ?>
<?php include('navbar.php') ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Calculus I - Limits - AITU MathAnalysis</title>
    <link rel="shortcut icon" href="stuff.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style media="screen">

          body {
          background-color: black;
          font-family: Arial;
          display: block;
          background-image: url(repeated-square-dark.png);
          background-position: center;
          padding: 0;
          margin: 0;
          }
          .sk-cube-grid {
            width: 100px;
            height: 100px;
            position:fixed;
            margin-left: 47%;
            margin-top: 30vh;
            animation: moving1 linear;
            animation-duration: 0.5s;
            animation-delay: 1.5s;
            animation-fill-mode: both;
            opacity: 0.8;
          }
          @keyframes moving1 {
              0% {
              opacity: 0.8; z-index: 1;
              }
              100% {
              opacity: 0; z-index: -1;
            }
          }
          @keyframes moving {
              0% {
              opacity: 0; z-index: -1;
              }
              100% {
              opacity: 1; z-index: 1;
            }
          }
          .contentdiv{
            animation: moving linear;
            animation-duration: 1s;
            animation-delay: 2s;
            animation-fill-mode: both;
          }
          .sk-cube-grid .sk-cube {
            border-radius: 5px;
            width: 33%;
            height: 33%;
            float: left;
            -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                    animation-iteration-count: 2;
          }
          .sk-cube-grid .sk-cube1 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                    background-color: blue; }
          .sk-cube-grid .sk-cube2 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                    background-color: lime;}
          .sk-cube-grid .sk-cube3 {
            -webkit-animation-delay: 0.4s;
                    animation-delay: 0.4s;
                  background-color: red;}
          .sk-cube-grid .sk-cube4 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: lightblue; }
          .sk-cube-grid .sk-cube5 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: purple; }
          .sk-cube-grid .sk-cube6 {
            -webkit-animation-delay: 0.3s;
                    animation-delay: 0.3s;
                  background-color: pink; }
          .sk-cube-grid .sk-cube7 {
            -webkit-animation-delay: 0s;
                    animation-delay: 0s;
                  background-color: yellow;}
          .sk-cube-grid .sk-cube8 {
            -webkit-animation-delay: 0.1s;
                    animation-delay: 0.1s;
                  background-color: orange; }
          .sk-cube-grid .sk-cube9 {
            -webkit-animation-delay: 0.2s;
                    animation-delay: 0.2s;
                  background-color: rgb(100,200,300);}

          @-webkit-keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }
          }

          @keyframes sk-cubeGridScaleDelay {
            0%, 70%, 100% {
              -webkit-transform: scale3D(1, 1, 1);
                      transform: scale3D(1, 1, 1);
            } 35% {
              -webkit-transform: scale3D(0, 0, 1);
                      transform: scale3D(0, 0, 1);
            }

          }
          .row{
            width: 100%;
            padding: 1%;
            display: flex;
            flex-direction: row;
            justify-content: space-around;
          }
          @keyframes fadeapp {
            from {opacity: 0;}
            to {opacity: 1;}
          }
          .imga{
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 3s;
            animation-delay: 2.5s;
            animation-fill-mode: forwards;
          }
          .aligncenter {
            text-align: center;
          }
          .textdiv {
            margin: 20px;
            opacity: 0;
            animation: fadeapp linear;
            animation-duration: 1s;
            animation-fill-mode: forwards;
            background-color: rgba(8, 43, 112, 0.5);
            color: white;
            border: white solid 3px;
            border-radius: 5px;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            padding: 10px;
            height: 70%;
          }
          .footer11{
            padding: 10px;
            background-color: rgb(38, 45, 66);
            width: 100%;
            color: rgb(88, 89, 92);
          }
          .logos{
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
          }
          .logo{
            margin-right: 20px;
          }
    </style>

  </head>
  <body>
    
    <div class="textdiv" style="margin-top:100px;">
      <h1 class="aligncenter">Limits</h1>
      <h4>Properties</h4>
      <div class="property">
        <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
        <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
        <p>First, we will assume that \(\mathop {\lim }\limits_{x \to a} f\left( x \right)\) and \(\mathop {\lim }\limits_{x \to a} g\left( x \right)\) exist and that \(c\) is any constant.  Then,</p>
        <ol>
          <li>\(\mathop {\lim }\limits_{x \to a} \left[ {cf\left( x \right)} \right] = c\mathop {\lim }\limits_{x \to a} f\left( x \right)\)
            <p>In other words, we can “factor” a multiplicative constant out of a limit.</p>
          </li>
          <li>\(\mathop {\lim }\limits_{x \to a} \left[ {f\left( x \right) \pm g\left( x \right)} \right] = \mathop {\lim }\limits_{x \to a} f\left( x \right) \pm \mathop {\lim }\limits_{x \to a} g\left( x \right)\)

            <p>So, to take the limit of a sum or difference all we need to do is take the limit of the individual parts and then put them back together with the appropriate sign.  This is also not limited to two functions.  This fact will work no matter how many functions we’ve got separated by “+” or “-”.</p>
          </li>
          <li>\(\mathop {\lim }\limits_{x \to a} \left[ {f\left( x \right)g\left( x \right)} \right] = \mathop {\lim }\limits_{x \to a} f\left( x \right)\,\,\,\mathop {\lim }\limits_{x \to a} g\left( x \right)\)

            <p>We take the limits of products in the same way that we can take the limit of sums or differences.  Just take the limit of the pieces and then put them back together.  Also, as with sums or differences, this fact is not limited to just two functions.</p>
          </li>
          <li>\(\displaystyle \mathop {\lim }\limits_{x \to a} \left[ {\frac{{f\left( x \right)}}{{g\left( x \right)}}} \right] = \frac{{\mathop {\lim }\limits_{x \to a} f\left( x \right)}}{{\mathop {\lim }\limits_{x \to a} g\left( x \right)}}{\rm{,}}\,\,\,\,\,{\rm{provided  }}\,\mathop {\lim }\limits_{x \to a} g\left( x \right) \ne 0\)

            <p>As noted in the statement we only need to worry about the limit in the denominator being zero when we do the limit of a quotient.  If it were zero we would end up with a division by zero error and we need to avoid that.</p>
          </li>
          <li>\(\mathop {\lim }\limits_{x \to a} {\left[ {f\left( x \right)} \right]^n} = {\left[ {\mathop {\lim }\limits_{x \to a} f\left( x \right)} \right]^n},\,\,\,\,{\mbox{where }}n{\mbox{ is any real number}}\)
            <p>In this property \(n\) can be any real number (positive, negative, integer, fraction, irrational, zero, <em>etc.</em>).  In the case that \(n\) is an integer this rule can be thought of as an extended case of <strong>3</strong>.</p>

            <p>For example, consider the case of \(n = \)2.</p>

              \[\begin{align*}\mathop {\lim }\limits_{x \to a} {\left[ {f\left( x \right)} \right]^2} & = \mathop {\lim }\limits_{x \to a} \left[ {f\left( x \right)f\left( x \right)} \right]\\ &  = \mathop {\lim }\limits_{x \to a} f\left( x \right)\mathop {\lim }\limits_{x \to a} f\left( x \right)\hspace{0.5in}{\mbox{using property 3}}\\ &  = {\left[ {\mathop {\lim }\limits_{x \to a} f\left( x \right)} \right]^2}\end{align*}\]

            <p>  The same can be done for any integer \(n\).
            </li>
            <li>\(\mathop {\lim }\limits_{x \to a} \left[ {\sqrt[n]{{f\left( x \right)}}} \right] = \sqrt[n]{{\mathop {\lim }\limits_{x \to a} f\left( x \right)}}\)

            <p>This is just a special case of the previous example.</p>

              \[\begin{align*}\mathop {\lim }\limits_{x \to a} \left[ {\sqrt[n]{{f\left( x \right)}}} \right] & = \mathop {\lim }\limits_{x \to a} {\left[ {f\left( x \right)} \right]^{\frac{1}{n}}}\\ &  = {\left[ {\mathop {\lim }\limits_{x \to a} f\left( x \right)} \right]^{\frac{1}{n}}}\\ &  = \sqrt[n]{{\mathop {\lim }\limits_{x \to a} f\left( x \right)}}\end{align*}\]
            </li>
            <li>\(\mathop {\lim }\limits_{x \to a} c = c,\,\,\,\,c{\mbox{ is any real number}}\)

            <p>In other words, the limit of a constant is just the constant.  You should be able to convince yourself of this by drawing the graph of \(f\left( x \right) = c\).</p>
            </li>
            <li>\(\mathop {\lim }\limits_{x \to a} x = a\)

            <p>As with the last one you should be able to convince yourself of this by drawing the graph of \(f\left( x \right) = x\).</p>
            </li>
            <li>\(\mathop {\lim }\limits_{x \to a} {x^n} = {a^n}\)

            <p>This is really just a special case of property <strong>5</strong> using \(f\left( x \right) = x\).</p>
            </li>
            </ol>
            <span class="example-title">Example 1</span> Compute the value of the following limit.

            \[\mathop {\lim }\limits_{x \to  - 2} \left( {3{x^2} + 5x - 9} \right)\]
            <button class="btn btn-primary" data-toggle="collapse" data-target="#demo1">Show Solution</button>

            <div id="demo1" class="collapse">
              <div class="textdiv" style="background-color: rgba(120, 70, 120, 0.4);">
                <p>This first time through we will use only the properties above to compute the limit.
          </p>

        <p>First, we will use property <strong>2</strong> to break up the limit into three separate limits.  We will then use property <strong>1</strong> to bring the constants out of the first two limits.  Doing this gives us,</p>

          \[\begin{align*}\mathop {\lim }\limits_{x \to  - 2} \left( {3{x^2} + 5x - 9} \right) & = \mathop {\lim }\limits_{x \to  - 2} 3{x^2} + \mathop {\lim }\limits_{x \to  - 2} 5x - \mathop {\lim }\limits_{x \to  - 2} 9\\ & = 3\mathop {\lim }\limits_{x \to  - 2} {x^2} + \mathop {5\lim }\limits_{x \to  - 2} x - \mathop {\lim }\limits_{x \to  - 2} 9\end{align*}\]

        <p>We can now use properties <strong>7</strong> through <strong>9</strong> to actually compute the limit.</p>

          \[\begin{align*}\mathop {\lim }\limits_{x \to  - 2} \left( {3{x^2} + 5x - 9} \right) & = 3\mathop {\lim }\limits_{x \to  - 2} {x^2} + \mathop {5\lim }\limits_{x \to  - 2} x - \mathop {\lim }\limits_{x \to  - 2} 9\\ &  = 3{\left( { - 2} \right)^2} + 5\left( { - 2} \right) - 9\\ &  =  - 7\end{align*}\]

              </div>
            </div>
          </div>
        </div>
        <div class="textdiv aligncenter">
          <div class="col-lg-12 col-md-12">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/kfF40MiS7zA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>

        </div>
        <div class="textdiv">
          <div class="col-lg-8 col-md-8" style="margin: auto;">
            <h1 class="aligncenter">Checkout photos from our lectures!</h1>
            <div id="demo" class="carousel slide" data-ride="carousel">

          <!-- Indicators -->
          <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
          </ul>

          <!-- The slideshow -->
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="lec1.jpg" alt="" width="100%" height="600px">
            </div>
            <div class="carousel-item">
              <img src="lec2.jpg" alt="" width="100%" height="600px">
            </div>
            <div class="carousel-item">
              <img src="lec3.jpg" alt="" width="100%" height="600px">
            </div>
          </div>

          <!-- Left and right controls -->
          <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
          </a>

        </div>
        </div>
        </div>

                <?php include('footer.php') ?>
  </body>
</html>
